//
//  UIColor+Extensions.m
//  urok
//
//  Created by Muhammad Ahsan on 6/15/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import "UIColor+Extensions.h"

@implementation UIColor (Extensions)

+ (UIColor *)urokBlueColor {
    return [UIColor colorWithRed:65.0f/ 255.0f green:184.0f / 255.0f blue:240.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ENTcolorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    //    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
@end
