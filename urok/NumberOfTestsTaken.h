//
//  DataTable.h
//  urok
//
//  Created by Muhammad Ahsan on 7/20/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import <Foundation/Foundation.h>

//@interface DataTable : NSObject

@interface NumberOfTestsTaken: NSObject

@property (nonatomic, strong) NSDate *dot;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSDate *updated;
@property (nonatomic) NSUInteger nscore;
@property (nonatomic) NSUInteger tnumber;

-(void)saveTestDetails;

@end

//@interface Contact2: NSObject
//@property (nonatomic, strong) NSString *objectId;
//@property (nonatomic, strong) NSDate *created;
//@property (nonatomic, strong) NSDate *updated;
//@property (nonatomic, strong) NSString *name;
//@property (nonatomic, strong) NSString *title;
//@property (nonatomic, strong) NSString *phone;
//@property (nonatomic) NSUInteger age;
//@end
//
//@implementation Contact2
//@end
