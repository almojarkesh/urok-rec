//
//  DataTable.m
//  urok
//
//  Created by Muhammad Ahsan on 7/20/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import "NumberOfTestsTaken.h"

@implementation NumberOfTestsTaken : NSObject

-(void)saveTestDetails
{

    NumberOfTestsTaken *contact = [NumberOfTestsTaken new];
    contact.updated = [NSDate date];
    contact.dot = [NSDate date];
    contact.created = [NSDate date];
    contact.tnumber = 1;
    id<IDataStore> dataStore = [backendless.persistenceService of:[NumberOfTestsTaken class]];
    [dataStore save:contact response:^(id response) {
        NSLog(@"%@", response);

        
    } error:^(Fault *fault) {
        NSLog(@"%@", fault.detail);

        
    }];
}

@end
