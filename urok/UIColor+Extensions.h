//
//  UIColor+Extensions.h
//  urok
//
//  Created by Muhammad Ahsan on 6/15/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extensions)

+ (UIColor *)urokBlueColor;

@end
