//
//  SoundManager.h
//  ATBasicSounds
//
//  Created by Audrey M Tam on 22/03/2014.
//  Copyright (c) 2014 Ray Wenderlich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundManager : NSObject

+ (instancetype)sharedInstance;
-(BOOL)isPlaying;
-(void)stopPlayer;
- (void)playSound:(NSString*)soundName;
- (void)playSound:(NSString*)soundName type:(NSString*)type;

@end
