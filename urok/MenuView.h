//
//  MenuView.h
//  urok
//
//  Created by Muhammad Ahsan on 6/16/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuView : UIView

@property (nonatomic, retain) IBOutlet UIView *view;
@property (nonatomic, strong) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UISwitch *voiceSwitch;
@property (nonatomic, copy) void(^switchChange)();

- (void)show;
- (IBAction)VoicSwitchAction:(id)sender;

@end
