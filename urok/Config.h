//
//  Config.h
//  urok
//
//  Created by Muhammad Ahsan on 7/24/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject

@property (nonatomic) BOOL listen_button_enable;

@end
