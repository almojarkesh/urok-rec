#include "DetectMgr.h"
#include "globals.h"
#import <math.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
//#import "Sound_Alert-Swift.h"
//#import "BraciPro-Swift.h"
//#import "Sound Alert-Bridging-Header.h"

using namespace std;
const float minFreqToDraw = 400; // min frequency to represent graphically
const float maxFreqToDraw = 5000; // max frequency to represent

const int MAX_GROUP_CNT = 2000;
const float RELATED_RATE = 1.f;

const int MAX_FREQ_CNT = 10;
const float fDeltaThreshold = 0.1f;


struct valueStruct
{
    int UNIVERSAL_THRESHOLDS ;
    
    int UNIVERSAL_MIN_FREQ ;
    int UNIVERSAL_MAX_FREQ ;
    int UNIVERSAL_MIN_PERIOD_FRAMES ;
    int UNIVERSAL_MAX_PERIOD_FRAMES ;
    int UNIVERSAL_MIN_STOP_FRAMES ;
    int UNIVERSAL_MAX_STOP_FRAMES ;
    int UNIVERSAL_MIN_REPEATS ;
    int UNIVERSAL_DETECT_PERIOD_FRAMES;
};

struct valueStruct valuesStr[122];

void setValuesInDetect(){
    
    valuesStr[0].UNIVERSAL_THRESHOLDS =  20; // p
    valuesStr[0].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[0].UNIVERSAL_MAX_FREQ =  1800;
    
    valuesStr[0].UNIVERSAL_MIN_PERIOD_FRAMES = 3;
    valuesStr[0].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
  //  valuesStr[0].UNIVERSAL_DETECT_PERIOD_FRAMES =  3;
    
    
    valuesStr[1].UNIVERSAL_THRESHOLDS =  25;  // b
    valuesStr[1].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[1].UNIVERSAL_MAX_FREQ =  1300;
    
    valuesStr[1].UNIVERSAL_MIN_PERIOD_FRAMES = 7;
    valuesStr[1].UNIVERSAL_MAX_PERIOD_FRAMES = 15;
    
  //  valuesStr[1].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
    
    valuesStr[2].UNIVERSAL_THRESHOLDS =  10; // t
    valuesStr[2].UNIVERSAL_MIN_FREQ =  600 ;
    valuesStr[2].UNIVERSAL_MAX_FREQ =  5000;
    
    valuesStr[2].UNIVERSAL_MIN_PERIOD_FRAMES = 6;
    valuesStr[2].UNIVERSAL_MAX_PERIOD_FRAMES = 12;
    
  //  valuesStr[2].UNIVERSAL_DETECT_PERIOD_FRAMES =  6;
    
    
    valuesStr[3].UNIVERSAL_THRESHOLDS =  50; // d
    valuesStr[3].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[3].UNIVERSAL_MAX_FREQ =  3400;
    
    valuesStr[3].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[3].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
  //  valuesStr[3].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
    
    valuesStr[4].UNIVERSAL_THRESHOLDS =  10;  //k
    valuesStr[4].UNIVERSAL_MIN_FREQ =  1400;
    valuesStr[4].UNIVERSAL_MAX_FREQ =  2500;
    
    valuesStr[4].UNIVERSAL_MIN_PERIOD_FRAMES = 3;
    valuesStr[4].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
   // valuesStr[4].UNIVERSAL_DETECT_PERIOD_FRAMES =  3;
    
    valuesStr[5].UNIVERSAL_THRESHOLDS =  5; //g hard sound
    valuesStr[5].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[5].UNIVERSAL_MAX_FREQ =  3100;
    
    valuesStr[5].UNIVERSAL_MIN_PERIOD_FRAMES = 5;
    valuesStr[5].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
   // valuesStr[5].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[6].UNIVERSAL_THRESHOLDS =  20; // g soft sound
    valuesStr[6].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[6].UNIVERSAL_MAX_FREQ =  3400;
    
    valuesStr[6].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[6].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
//    valuesStr[6].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
//    valuesStr[7].UNIVERSAL_THRESHOLDS =  10; // c hard sound
//    valuesStr[7].UNIVERSAL_MIN_FREQ =  1400;
//    valuesStr[7].UNIVERSAL_MAX_FREQ =  2500;
//
//    valuesStr[7].UNIVERSAL_MIN_PERIOD_FRAMES = 3;
//    valuesStr[7].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
  //  valuesStr[7].UNIVERSAL_DETECT_PERIOD_FRAMES =  3;
    
    valuesStr[8].UNIVERSAL_THRESHOLDS =  3; // c soft sound
    valuesStr[8].UNIVERSAL_MIN_FREQ =  1400;
    valuesStr[8].UNIVERSAL_MAX_FREQ =  5000;
    
    valuesStr[8].UNIVERSAL_MIN_PERIOD_FRAMES = 10;
    valuesStr[8].UNIVERSAL_MAX_PERIOD_FRAMES = 20;
    
 //   valuesStr[8].UNIVERSAL_DETECT_PERIOD_FRAMES =  10;
    
    valuesStr[9].UNIVERSAL_THRESHOLDS =  3; // f
    valuesStr[9].UNIVERSAL_MIN_FREQ =  1300;
    valuesStr[9].UNIVERSAL_MAX_FREQ =  3700;
    
    valuesStr[9].UNIVERSAL_MIN_PERIOD_FRAMES = 10;
    valuesStr[9].UNIVERSAL_MAX_PERIOD_FRAMES = 20;
    
  //  valuesStr[9].UNIVERSAL_DETECT_PERIOD_FRAMES =  10;
    
//    valuesStr[10].UNIVERSAL_THRESHOLDS =  3; // ph
//    valuesStr[10].UNIVERSAL_MIN_FREQ =  1300;
//    valuesStr[10].UNIVERSAL_MAX_FREQ =  3700;
//
//    valuesStr[10].UNIVERSAL_MIN_PERIOD_FRAMES = 10;
//    valuesStr[10].UNIVERSAL_MAX_PERIOD_FRAMES = 20;
    
 //   valuesStr[10].UNIVERSAL_DETECT_PERIOD_FRAMES =  10;
    
//    valuesStr[11].UNIVERSAL_THRESHOLDS =  3; // gh ex: tough
//    valuesStr[11].UNIVERSAL_MIN_FREQ =  1300;
//    valuesStr[11].UNIVERSAL_MAX_FREQ =  3700;
//
//    valuesStr[11].UNIVERSAL_MIN_PERIOD_FRAMES = 10;
//    valuesStr[11].UNIVERSAL_MAX_PERIOD_FRAMES = 20;
    
 //   valuesStr[11].UNIVERSAL_DETECT_PERIOD_FRAMES =  10;
    
//    valuesStr[12].UNIVERSAL_THRESHOLDS =  5; // gh ex: ghost
//    valuesStr[12].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[12].UNIVERSAL_MAX_FREQ =  3100;
//
//    valuesStr[12].UNIVERSAL_MIN_PERIOD_FRAMES = 5;
//    valuesStr[12].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
 //   valuesStr[12].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[13].UNIVERSAL_THRESHOLDS =  5; // v
    valuesStr[13].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[13].UNIVERSAL_MAX_FREQ =  600;
    
    valuesStr[13].UNIVERSAL_MIN_PERIOD_FRAMES = 8;
    valuesStr[13].UNIVERSAL_MAX_PERIOD_FRAMES = 18;
    
 //   valuesStr[13].UNIVERSAL_DETECT_PERIOD_FRAMES =  8;
    
    valuesStr[14].UNIVERSAL_THRESHOLDS =  10;  // th noisy sound
    valuesStr[14].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[14].UNIVERSAL_MAX_FREQ =  3000;
    
    valuesStr[14].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[14].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
  //  valuesStr[14].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[15].UNIVERSAL_THRESHOLDS =  5;  // th quiet sound
    valuesStr[15].UNIVERSAL_MIN_FREQ =  1500;
    valuesStr[15].UNIVERSAL_MAX_FREQ =  5000;
   
    valuesStr[15].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[15].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
    //valuesStr[15].UNIVERSAL_DETECT_PERIOD_FRAMES =  8;
    
//    valuesStr[16].UNIVERSAL_THRESHOLDS =  3; // s soft sound
//    valuesStr[16].UNIVERSAL_MIN_FREQ =  1400;
//    valuesStr[16].UNIVERSAL_MAX_FREQ =  5000;
//
//    valuesStr[16].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[16].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[16].UNIVERSAL_DETECT_PERIOD_FRAMES =  3;
    
    valuesStr[17].UNIVERSAL_THRESHOLDS =  3; // s hard sound
    valuesStr[17].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[17].UNIVERSAL_MAX_FREQ =  1800;
    
    valuesStr[17].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[17].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
  //  valuesStr[17].UNIVERSAL_DETECT_PERIOD_FRAMES =  6;
    
//    valuesStr[18].UNIVERSAL_THRESHOLDS =  3; // z
//    valuesStr[18].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[18].UNIVERSAL_MAX_FREQ =  1800;
//
//    valuesStr[18].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[18].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
    
  //  valuesStr[18].UNIVERSAL_DETECT_PERIOD_FRAMES =  6;
    
    valuesStr[19].UNIVERSAL_THRESHOLDS =  5; //sh
    valuesStr[19].UNIVERSAL_MIN_FREQ =  2000;
    valuesStr[19].UNIVERSAL_MAX_FREQ =  5000;
    
    valuesStr[19].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[19].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[19].UNIVERSAL_DETECT_PERIOD_FRAMES =  10;
    
    valuesStr[20].UNIVERSAL_THRESHOLDS =  10; // ch ex:chip
    valuesStr[20].UNIVERSAL_MIN_FREQ =  2000;
    valuesStr[20].UNIVERSAL_MAX_FREQ =  3100;
    
    valuesStr[20].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[20].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[20].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[21].UNIVERSAL_THRESHOLDS =  10; // ch ex:chrome
//    valuesStr[21].UNIVERSAL_MIN_FREQ =  1400;
//    valuesStr[21].UNIVERSAL_MAX_FREQ =  2500;
//    valuesStr[21].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[21].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[21].UNIVERSAL_DETECT_PERIOD_FRAMES =  3;
    
//    valuesStr[22].UNIVERSAL_THRESHOLDS =  5; // ch ex:chef
//    valuesStr[22].UNIVERSAL_MIN_FREQ =  2000;
//    valuesStr[22].UNIVERSAL_MAX_FREQ =  5000;
//    valuesStr[22].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[22].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
   // valuesStr[22].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[23].UNIVERSAL_THRESHOLDS =  20;  // j
//    valuesStr[23].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[23].UNIVERSAL_MAX_FREQ =  3400;
//    valuesStr[23].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
//
    valuesStr[24].UNIVERSAL_THRESHOLDS =  20; // l
    valuesStr[24].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[24].UNIVERSAL_MAX_FREQ =  700;
    valuesStr[24].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[24].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//    valuesStr[24].UNIVERSAL_DETECT_PERIOD_FRAMES =  12;
    
    valuesStr[25].UNIVERSAL_THRESHOLDS =  40; // r
    valuesStr[25].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[25].UNIVERSAL_MAX_FREQ =  1900;
    valuesStr[25].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[25].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//    valuesStr[25].UNIVERSAL_DETECT_PERIOD_FRAMES =  15;
    
//    valuesStr[26].UNIVERSAL_THRESHOLDS =  40; // wr
//    valuesStr[26].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[26].UNIVERSAL_MAX_FREQ =  1900;
//    valuesStr[26].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[26].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[26].UNIVERSAL_DETECT_PERIOD_FRAMES =  15;
    
    valuesStr[27].UNIVERSAL_THRESHOLDS =  10; // w
    valuesStr[27].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[27].UNIVERSAL_MAX_FREQ =  1600;
    valuesStr[27].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[27].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[27].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
    valuesStr[28].UNIVERSAL_THRESHOLDS =  20; // h
    valuesStr[28].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[28].UNIVERSAL_MAX_FREQ =  3300;
    valuesStr[28].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[28].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[28].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
    valuesStr[29].UNIVERSAL_THRESHOLDS =  10; // wh
    valuesStr[29].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[29].UNIVERSAL_MAX_FREQ =  1600;
    valuesStr[29].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[29].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[29].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
    valuesStr[30].UNIVERSAL_THRESHOLDS =  10; // m
    valuesStr[30].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[30].UNIVERSAL_MAX_FREQ =  500;
    valuesStr[30].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[30].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[30].UNIVERSAL_DETECT_PERIOD_FRAMES =  8;
    
    valuesStr[31].UNIVERSAL_THRESHOLDS =  10; // n
    valuesStr[31].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[31].UNIVERSAL_MAX_FREQ =  500;
    valuesStr[31].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[31].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[31].UNIVERSAL_DETECT_PERIOD_FRAMES =  8;
    
//    valuesStr[32].UNIVERSAL_THRESHOLDS =  15; // kn
//    valuesStr[32].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[32].UNIVERSAL_MAX_FREQ =  500;
//    valuesStr[32].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[32].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[32].UNIVERSAL_DETECT_PERIOD_FRAMES =  20;
    
    valuesStr[33].UNIVERSAL_THRESHOLDS =  15; // ng
    valuesStr[33].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[33].UNIVERSAL_MAX_FREQ =  500;
    valuesStr[33].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[33].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[33].UNIVERSAL_DETECT_PERIOD_FRAMES =  20;
    
    valuesStr[34].UNIVERSAL_THRESHOLDS =  15; // nk
    valuesStr[34].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[34].UNIVERSAL_MAX_FREQ =  500;
    valuesStr[34].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[34].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//    valuesStr[34].UNIVERSAL_DETECT_PERIOD_FRAMES =  6;
    
    valuesStr[35].UNIVERSAL_THRESHOLDS =  10; // qu
    valuesStr[35].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[35].UNIVERSAL_MAX_FREQ =  1500;
    valuesStr[35].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[35].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[35].UNIVERSAL_DETECT_PERIOD_FRAMES =  8;
    
    valuesStr[36].UNIVERSAL_THRESHOLDS =  100; // y (consonant) ex: yes
    valuesStr[36].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[36].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[36].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[36].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[36].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[37].UNIVERSAL_THRESHOLDS =  5;  // x ex:box
    valuesStr[37].UNIVERSAL_MIN_FREQ =  1500;
    valuesStr[37].UNIVERSAL_MAX_FREQ =  5000;
    valuesStr[37].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[37].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[37].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
    
    
//    valuesStr[38].UNIVERSAL_THRESHOLDS =  3; // x ex:xylophone
//    valuesStr[38].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[38].UNIVERSAL_MAX_FREQ =  1800;
//    valuesStr[38].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[38].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[38].UNIVERSAL_DETECT_PERIOD_FRAMES =  8;
    
    valuesStr[39].UNIVERSAL_THRESHOLDS =  10; // x ex:exam
    valuesStr[39].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[39].UNIVERSAL_MAX_FREQ =  3000;
    valuesStr[39].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[39].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[39].UNIVERSAL_DETECT_PERIOD_FRAMES =  8;
    
//    valuesStr[40].UNIVERSAL_THRESHOLDS =  10; // ck
//    valuesStr[40].UNIVERSAL_MIN_FREQ =  1400;
//    valuesStr[40].UNIVERSAL_MAX_FREQ =  2500;
//    valuesStr[40].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[40].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[40].UNIVERSAL_DETECT_PERIOD_FRAMES =  3;
    
//    valuesStr[41].UNIVERSAL_THRESHOLDS =  10; // tch
//    valuesStr[41].UNIVERSAL_MIN_FREQ =  2000;
//    valuesStr[41].UNIVERSAL_MAX_FREQ =  3100;
//    valuesStr[41].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[41].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[41].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[42].UNIVERSAL_THRESHOLDS =  20; // dge
//    valuesStr[42].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[42].UNIVERSAL_MAX_FREQ =  600;
//    valuesStr[42].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
    valuesStr[43].UNIVERSAL_THRESHOLDS =  20; // i short
    valuesStr[43].UNIVERSAL_MIN_FREQ =  400;
    valuesStr[43].UNIVERSAL_MAX_FREQ =  2300;
    valuesStr[43].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[43].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[43].UNIVERSAL_DETECT_PERIOD_FRAMES =  6;
    
    valuesStr[44].UNIVERSAL_THRESHOLDS =  10; // i long
    valuesStr[44].UNIVERSAL_MIN_FREQ =  400;
    valuesStr[44].UNIVERSAL_MAX_FREQ =  2300;
    valuesStr[44].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
    valuesStr[45].UNIVERSAL_THRESHOLDS =  30; // e short
    valuesStr[45].UNIVERSAL_MIN_FREQ =  500;
    valuesStr[45].UNIVERSAL_MAX_FREQ =  1000;
    valuesStr[45].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[45].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[45].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[46].UNIVERSAL_THRESHOLDS =  100; // e long
    valuesStr[46].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[46].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[46].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[47].UNIVERSAL_THRESHOLDS =  30; // a short
    valuesStr[47].UNIVERSAL_MIN_FREQ =  600;
    valuesStr[47].UNIVERSAL_MAX_FREQ =  1600;
    valuesStr[47].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[47].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[47].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
    valuesStr[48].UNIVERSAL_THRESHOLDS =  30; // a long
    valuesStr[48].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[48].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[48].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    
    valuesStr[49].UNIVERSAL_THRESHOLDS =  30; // u short
    valuesStr[49].UNIVERSAL_MIN_FREQ =  700;
    valuesStr[49].UNIVERSAL_MAX_FREQ =  1400;
    valuesStr[49].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[49].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[49].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
    valuesStr[50].UNIVERSAL_THRESHOLDS =  30; // u long
    valuesStr[50].UNIVERSAL_MIN_FREQ =  500;
    valuesStr[50].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[50].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
    
    valuesStr[51].UNIVERSAL_THRESHOLDS =  30; // o short
    valuesStr[51].UNIVERSAL_MIN_FREQ =  600;
    valuesStr[51].UNIVERSAL_MAX_FREQ =  1200;
    valuesStr[51].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[51].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[51].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
    
    valuesStr[52].UNIVERSAL_THRESHOLDS =  100; // o long
    valuesStr[52].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[52].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[52].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[53].UNIVERSAL_THRESHOLDS =  10; // y ex:gym
//    valuesStr[53].UNIVERSAL_MIN_FREQ =  400;
//    valuesStr[53].UNIVERSAL_MAX_FREQ =  2300;
//    valuesStr[53].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[53].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[53].UNIVERSAL_DETECT_PERIOD_FRAMES =  4;
    
//    valuesStr[54].UNIVERSAL_THRESHOLDS =  100; // y ex:fly
//    valuesStr[54].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[54].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[54].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[54].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[54].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[55].UNIVERSAL_THRESHOLDS =  100; // y ex:windy
//    valuesStr[55].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[55].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[55].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[55].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[55].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[56].UNIVERSAL_THRESHOLDS =  100; // ble
    valuesStr[56].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[56].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[56].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[57].UNIVERSAL_THRESHOLDS =  100; // dle
    valuesStr[57].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[57].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[57].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    
    valuesStr[58].UNIVERSAL_THRESHOLDS =  100; // ple
    valuesStr[58].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[58].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[58].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[59].UNIVERSAL_THRESHOLDS =  100; // tle
    valuesStr[59].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[59].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[59].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    
//    valuesStr[60].UNIVERSAL_THRESHOLDS =  20; // tle silent ’t’
//    valuesStr[60].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[60].UNIVERSAL_MAX_FREQ =  700;
//    valuesStr[60].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[60].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[60].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[61].UNIVERSAL_THRESHOLDS =  100; // gle
    valuesStr[61].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[61].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[61].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[62].UNIVERSAL_THRESHOLDS =  100; // fle
    valuesStr[62].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[62].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[62].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    
    valuesStr[63].UNIVERSAL_THRESHOLDS =  100; // kle
    valuesStr[63].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[63].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[63].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[64].UNIVERSAL_THRESHOLDS =  100; // cle
//    valuesStr[64].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[64].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[64].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[65].UNIVERSAL_THRESHOLDS =  100; // zle
    valuesStr[65].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[65].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[65].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[66].UNIVERSAL_THRESHOLDS =  10; // or ex:for
    valuesStr[66].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[66].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[66].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[67].UNIVERSAL_THRESHOLDS =  10; // or ex:doctor
    valuesStr[67].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[67].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[67].UNIVERSAL_MIN_PERIOD_FRAMES = 2;
    valuesStr[67].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[67].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[68].UNIVERSAL_THRESHOLDS =  10; // ur
//    valuesStr[68].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[68].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[68].UNIVERSAL_MIN_PERIOD_FRAMES = 2;
//    valuesStr[68].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//    valuesStr[68].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[69].UNIVERSAL_THRESHOLDS =  10; // ir
//    valuesStr[69].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[69].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[69].UNIVERSAL_MIN_PERIOD_FRAMES = 2;
//    valuesStr[69].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[69].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[70].UNIVERSAL_THRESHOLDS =  10; // er
//    valuesStr[70].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[70].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[70].UNIVERSAL_MIN_PERIOD_FRAMES = 2;
//    valuesStr[70].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[70].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[71].UNIVERSAL_THRESHOLDS =  10; // ar ex:star
    valuesStr[71].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[71].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[71].UNIVERSAL_MIN_PERIOD_FRAMES = 2;
    valuesStr[71].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
   // valuesStr[71].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[72].UNIVERSAL_THRESHOLDS =  10; // ar ex:dollar
//    valuesStr[72].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[72].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[72].UNIVERSAL_MIN_PERIOD_FRAMES = 2;
//    valuesStr[72].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[72].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[73].UNIVERSAL_THRESHOLDS =  10; // ar ex:warm
//    valuesStr[73].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[73].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[73].UNIVERSAL_MIN_PERIOD_FRAMES = 2;
//    valuesStr[73].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
   // valuesStr[73].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[74].UNIVERSAL_THRESHOLDS =  10; // oi
    valuesStr[74].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[74].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[74].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    
//    valuesStr[75].UNIVERSAL_THRESHOLDS =  10; // oy
//    valuesStr[75].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[75].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[75].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[76].UNIVERSAL_THRESHOLDS =  30; // au
    valuesStr[76].UNIVERSAL_MIN_FREQ =  600;
    valuesStr[76].UNIVERSAL_MAX_FREQ =  1200;
    valuesStr[76].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[76].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[76].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
    
//    valuesStr[77].UNIVERSAL_THRESHOLDS =  30; // aw
//    valuesStr[77].UNIVERSAL_MIN_FREQ =  600;
//    valuesStr[77].UNIVERSAL_MAX_FREQ =  1200;
//    valuesStr[77].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[77].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
   // valuesStr[77].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
    
    valuesStr[78].UNIVERSAL_THRESHOLDS =  25; // ou ex:out
    valuesStr[78].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[78].UNIVERSAL_MAX_FREQ =  1500;
    valuesStr[78].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[78].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[78].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
    
    valuesStr[79].UNIVERSAL_THRESHOLDS =  25; // ou ex:soup
    valuesStr[79].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[79].UNIVERSAL_MAX_FREQ =  700;
    valuesStr[79].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[79].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
 //   valuesStr[79].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[80].UNIVERSAL_THRESHOLDS =  25; // ow ex:cow
//    valuesStr[80].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[80].UNIVERSAL_MAX_FREQ =  1800;
//    valuesStr[80].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[80].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[80].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
    
//    valuesStr[81].UNIVERSAL_THRESHOLDS =  25; // ow ex:show
//    valuesStr[81].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[81].UNIVERSAL_MAX_FREQ =  1800;
//    valuesStr[81].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[81].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[81].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
    
    valuesStr[82].UNIVERSAL_THRESHOLDS =  25; // oo ex:book
    valuesStr[82].UNIVERSAL_MIN_FREQ =  200;
    valuesStr[82].UNIVERSAL_MAX_FREQ =  700;
    valuesStr[82].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[82].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[82].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[83].UNIVERSAL_THRESHOLDS =  25; // oo ex:moon
//    valuesStr[83].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[83].UNIVERSAL_MAX_FREQ =  700;
//    valuesStr[83].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[83].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//   // valuesStr[83].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
//
//    valuesStr[84].UNIVERSAL_THRESHOLDS =  25; // u_e ex:flute
//    valuesStr[84].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[84].UNIVERSAL_MAX_FREQ =  1000;
//    valuesStr[84].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[84].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
// //   valuesStr[84].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
//
//    valuesStr[85].UNIVERSAL_THRESHOLDS =  25; // u_e ex:cute
//    valuesStr[85].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[85].UNIVERSAL_MAX_FREQ =  1000;
//    valuesStr[85].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[85].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
// //   valuesStr[85].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
//
//    valuesStr[86].UNIVERSAL_THRESHOLDS =  25; // ui
//    valuesStr[86].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[86].UNIVERSAL_MAX_FREQ =  1000;
//    valuesStr[86].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[86].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//  //  valuesStr[86].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
//
//    valuesStr[87].UNIVERSAL_THRESHOLDS =  25; // ue ex:blue
//    valuesStr[87].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[87].UNIVERSAL_MAX_FREQ =  1000;
//    valuesStr[87].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[87].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//  //  valuesStr[87].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
//
//
//    valuesStr[88].UNIVERSAL_THRESHOLDS =  25; // ue ex:cue
//    valuesStr[88].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[88].UNIVERSAL_MAX_FREQ =  1000;
//    valuesStr[88].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[88].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
// //   valuesStr[88].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
//
//    valuesStr[89].UNIVERSAL_THRESHOLDS =  25; // ew ex:flew
//    valuesStr[89].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[89].UNIVERSAL_MAX_FREQ =  1000;
//    valuesStr[89].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[89].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//  //  valuesStr[89].UNIVERSAL_DETECT_PERIOD_FRAMES =  8;
//
//    valuesStr[90].UNIVERSAL_THRESHOLDS =  25; // ew ex:few
//    valuesStr[90].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[90].UNIVERSAL_MAX_FREQ =  1000;
//    valuesStr[90].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[90].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[90].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[91].UNIVERSAL_THRESHOLDS =  100; // pre
    valuesStr[91].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[91].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[91].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[92].UNIVERSAL_THRESHOLDS =  100; // be
    valuesStr[92].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[92].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[92].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[93].UNIVERSAL_THRESHOLDS =  100; // un
    valuesStr[93].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[93].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[93].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[94].UNIVERSAL_THRESHOLDS =  100; // dis
    valuesStr[94].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[94].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[94].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[95].UNIVERSAL_THRESHOLDS =  100; // under
    valuesStr[95].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[95].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[95].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[96].UNIVERSAL_THRESHOLDS =  100; // sub
    valuesStr[96].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[96].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[96].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[97].UNIVERSAL_THRESHOLDS =  100; // in
    valuesStr[97].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[97].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[97].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[98].UNIVERSAL_THRESHOLDS =  100; // mis
    valuesStr[98].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[98].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[98].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[99].UNIVERSAL_THRESHOLDS =  100; // out
    valuesStr[99].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[99].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[99].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    
    valuesStr[100].UNIVERSAL_THRESHOLDS =  100; // fore
    valuesStr[100].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[100].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[100].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[101].UNIVERSAL_THRESHOLDS =  100; // de
    valuesStr[101].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[101].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[101].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[102].UNIVERSAL_THRESHOLDS =  100; // re
    valuesStr[102].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[102].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[102].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[103].UNIVERSAL_THRESHOLDS =  100; // ly
    valuesStr[103].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[103].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[103].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[104].UNIVERSAL_THRESHOLDS =  100; // less
    valuesStr[104].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[104].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[104].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[105].UNIVERSAL_THRESHOLDS =  100; // ment
    valuesStr[105].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[105].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[105].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[106].UNIVERSAL_THRESHOLDS =  100; // ful
    valuesStr[106].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[106].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[106].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[107].UNIVERSAL_THRESHOLDS =  100; // ness
    valuesStr[107].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[107].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[107].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[108].UNIVERSAL_THRESHOLDS =  100; // en
    valuesStr[108].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[108].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[108].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[109].UNIVERSAL_THRESHOLDS =  100; // ing
    valuesStr[109].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[109].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[109].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[110].UNIVERSAL_THRESHOLDS =  100; // est
    valuesStr[110].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[110].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[110].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    
    valuesStr[111].UNIVERSAL_THRESHOLDS =  100; // able
    valuesStr[111].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[111].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[111].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    
    valuesStr[112].UNIVERSAL_THRESHOLDS =  100; // age
    valuesStr[112].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[112].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[112].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[113].UNIVERSAL_THRESHOLDS =  100; // ed ex:landed
    valuesStr[113].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[113].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[113].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
    valuesStr[113].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[113].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[114].UNIVERSAL_THRESHOLDS =  25; // ed ex:snowed
//    valuesStr[114].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[114].UNIVERSAL_MAX_FREQ =  1300;
//    valuesStr[114].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[114].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//   // valuesStr[114].UNIVERSAL_DETECT_PERIOD_FRAMES =  7;
//
//    valuesStr[115].UNIVERSAL_THRESHOLDS =  25; // ed ex:fixed
//    valuesStr[115].UNIVERSAL_MIN_FREQ =  200;
//    valuesStr[115].UNIVERSAL_MAX_FREQ =  1300;
//    valuesStr[115].UNIVERSAL_MIN_PERIOD_FRAMES = 4;
//    valuesStr[115].UNIVERSAL_MAX_PERIOD_FRAMES = 10;
  //  valuesStr[115].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[116].UNIVERSAL_THRESHOLDS =  100; // tion
    valuesStr[116].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[116].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[116].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
//    valuesStr[117].UNIVERSAL_THRESHOLDS =  100; // sion ex:mansion
//    valuesStr[117].UNIVERSAL_MIN_FREQ =  1320;
//    valuesStr[117].UNIVERSAL_MAX_FREQ =  3650;
//    valuesStr[117].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[118].UNIVERSAL_THRESHOLDS =  100; // sion ex:division
    valuesStr[118].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[118].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[118].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[119].UNIVERSAL_THRESHOLDS =  100; // ture
    valuesStr[119].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[119].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[119].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[120].UNIVERSAL_THRESHOLDS =  100; // ous
    valuesStr[120].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[120].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[120].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    
    valuesStr[121].UNIVERSAL_THRESHOLDS =  100; // es
    valuesStr[121].UNIVERSAL_MIN_FREQ =  1320;
    valuesStr[121].UNIVERSAL_MAX_FREQ =  3650;
    valuesStr[121].UNIVERSAL_DETECT_PERIOD_FRAMES =  5;
    


}


CDetectMgr::CDetectMgr(int p_nSamplingFreq)
{
	m_fGroupAvgAmps = new float[MAX_GROUP_CNT];

	fft = new FFT(FRAME_LEN, (float)p_nSamplingFreq); //m_wavReader.m_Info.SamplingFreq
	fft->init();

	minIdx = fft->freqToIndex(minFreqToDraw);
	maxIdx = fft->freqToIndex(maxFreqToDraw);

	m_nDetectedFrames = 0;
    
    m_bDetectSmokeAlarmOnly = false;
    
    m_pFileRecording = nullptr;
    m_nRecordingFrames = 0;
    
    m_lpFuncRecording = nullptr;
    m_pObj = nullptr;
    
    m_bRecording = false;
    
    ResetFrameInfo();
    
//    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    m_pFreqIndicesToDraw = nullptr;
    m_pRealValsToDraw = nullptr;
    
    setValuesInDetect();
    
}

//int UNIVERSAL_MAX_PERIOD_FRAMES = 10;
//int UNIVERSAL_MIN_STOP_FRAMES = 5;



CDetectMgr::~CDetectMgr(void)
{
    if (m_pFileRecording != nullptr)
    {
        fclose(m_pFileRecording);
        m_pFileRecording = nullptr;
    }
    
    if (m_pFreqIndicesToDraw != nullptr) {
        delete [] m_pFreqIndicesToDraw;
        m_pFreqIndicesToDraw = nullptr;
    }
    
    if (m_pRealValsToDraw != nullptr) {
        delete [] m_pRealValsToDraw;
        m_pRealValsToDraw = nullptr;
    }
    
	ClearFftValues();
	delete fft;
	delete[] m_fGroupAvgAmps;
}

void CDetectMgr::ResetFrameInfo()
{
    Valid_Frames = 0;
    Invalid_Frames = 0;
    Repeat_Frames = 0;
    
}

void CDetectMgr::SetDetectSmokeAlarmOnly(bool bEnabled)
{
    m_bDetectSmokeAlarmOnly = bEnabled;
}

void CDetectMgr::ClearFftValues()
{
	for (std::vector<float*>::iterator iter = m_vecFftVals.begin(); iter != m_vecFftVals.end(); iter++) 
	{
		float* pVals = *iter;
		delete[] pVals;
	}
	m_vecFftVals.clear();

	m_nDetectedFrames = 0;
}

void CDetectMgr::RecordStart(const char* lpszFilePath, LPFUNC_RECORDING lpFuncRecording, void* pObj)
{
    m_pFileRecording = fopen(lpszFilePath, "wb");
    if (m_pFileRecording == nullptr)
        return;
    
    m_bRecording = false;
    
    m_lpFuncRecording = lpFuncRecording;
    m_pObj = pObj;
    
    fprintf(m_pFileRecording, "%d\t%d\n", minIdx, maxIdx);
    
    m_nRecordingFrames = 0;
}

void CDetectMgr::RecordStop()
{
    if (m_pFileRecording == nullptr)
        return;

    fclose(m_pFileRecording);
    m_pFileRecording = nullptr;
    
    m_bRecording = false;
    
    if (m_lpFuncRecording != nullptr)
        m_lpFuncRecording(m_pObj, RECORD_STOP, 0);
    m_lpFuncRecording = nullptr;
    
}

void CDetectMgr::StopRecording(const char* lpszFilePath,LPFUNC_RECORDING lpFuncRecording){ // this function stops recording without storing any file
  //  m_pFileRecording = fopen(lpszFilePath, "wb");
//    if  (m_pFileRecording != nullptr) {
//        printf("m_pFileRecording is not NULL");
//    }
//    if (m_pFileRecording != nullptr)
//        fclose(m_pFileRecording);
//    
//    
//    m_pFileRecording = nullptr;
//    
//    m_bRecording = false;
//    
//    if (m_lpFuncRecording != nullptr)
//        m_lpFuncRecording(m_pObj, RECORD_STOP, 0);
//    m_lpFuncRecording = nullptr;
    
    
    m_fGroupAvgAmps = new float[MAX_GROUP_CNT];
    int SAMPLE_FREQ = 44100;
    fft = new FFT(FRAME_LEN, (float)SAMPLE_FREQ); //m_wavReader.m_Info.SamplingFreq
    fft->init();
    
    minIdx = fft->freqToIndex(minFreqToDraw);
    maxIdx = fft->freqToIndex(maxFreqToDraw);
    
    m_nDetectedFrames = 0;
    
    m_bDetectSmokeAlarmOnly = false;
    
    m_pFileRecording = nullptr;
    m_nRecordingFrames = 0;
    
    m_lpFuncRecording = nullptr;
    m_pObj = nullptr;
    
    m_bRecording = false;
    
    ResetFrameInfo();
    
    //    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    m_pFreqIndicesToDraw = nullptr;
    m_pRealValsToDraw = nullptr;
    

   
    
}

bool CDetectMgr::CheckMatched(CDetectingData* p_DetectData, bool bMachineSound, float p_fMatchThreshold)
{
    if (p_DetectData == nullptr || p_DetectData->m_vecCommonData == nullptr || p_DetectData->m_vecCommonData->size() == 0)
		return false;
	if (m_vecFftVals.size() < p_DetectData->m_vecCommonData->size())
		return false;
    float fThreshold = p_fMatchThreshold;
    
    if (fThreshold < 0) {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"falseSoundAlerts"])
        {
            fThreshold = 0.93f;
        }
        else
        {
            fThreshold = MATCH_RATE_THRESHOLD;
        }
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"missingAlarmSound"])
        {
            fThreshold = 0.87f;
        }
        else
        {
            fThreshold = MATCH_RATE_THRESHOLD;
        }
       
    }

	unsigned long nDataIdx = m_vecFftVals.size() - 1;
	unsigned long nDetectIdx = p_DetectData->m_vecCommonData->size() - 1;
	int nTotalMatchedCnt = 0;

	for (unsigned int i = 0; i < p_DetectData->m_vecCommonData->size(); i++) {
		vector<AmpInfo>* pDetectAmpInfos = (vector<AmpInfo>*) p_DetectData->m_vecCommonData->at(nDetectIdx);
		float* fVals = m_vecFftVals.at(nDataIdx);
		nDetectIdx--;
		nDataIdx--;

		if (pDetectAmpInfos->size() == 0)
			continue;

		int nCnt = GetMatchedCount(fVals, pDetectAmpInfos, p_fMatchThreshold,
			bMachineSound);
        if (nCnt < 0){
            
			return false;
        }
		nTotalMatchedCnt += nCnt;
	}

	float matchRate = ((float) nTotalMatchedCnt) / p_DetectData->m_nTotalValidCnt;
    
    if (matchRate >= 0.60) {
    printf("Match rate %f And Threshhold %f" ,matchRate,fThreshold);
        printf("validCount before MatchRate %d\n",p_DetectData->m_nTotalValidCnt);
    }
    
	if (matchRate >= fThreshold)
		return true;

	return false;
}

int CDetectMgr::GetMatchedCount(float* p_fVals, vector<AmpInfo>* p_lstDetectAmpInfos, float p_fThreshold, bool bMachineSound) 
{
	int SEARCH_WIDTH = 3;
	int nGroupIdx = 0;
	int nPreviousIdx = 0;
	float fExtraSum = 0;
	int nExtraCnt = 0;
	unsigned long nSize = p_lstDetectAmpInfos->size();
	for (int i = 0; i < nSize; i++) 
	{
		int nFFTIdx = p_lstDetectAmpInfos->at(i).nFFtIdx;
		if (nFFTIdx - nPreviousIdx > 5 || i == nSize - 1) 
		{
			if (nPreviousIdx > 0) 
			{
				for (int j = 0; j < SEARCH_WIDTH; j++) 
				{
					int idx = nPreviousIdx + 1 + j;
					if (idx < maxIdx) 
					{
						fExtraSum += p_fVals[idx];
						nExtraCnt++;
					}
				}
				m_fGroupAvgAmps[nGroupIdx - 1] = fExtraSum / nExtraCnt;
			}

			fExtraSum = 0;
			nExtraCnt = 0;
			for (int j = 0; j < SEARCH_WIDTH; j++) 
			{
				int idx = nFFTIdx - SEARCH_WIDTH + j;
				fExtraSum += p_fVals[idx];
				nExtraCnt++;
			}

			nGroupIdx++;
		}
		nPreviousIdx = nFFTIdx;
	}

	nPreviousIdx = 0;
	nGroupIdx = 0;
	int nMatchedCnt = 0;
	int nGroupCnt = 0;
	int nGroupMatchedCnt = 0;
	for (int i = 0; i < nSize; i++) 
	{
		int nFFTIdx = p_lstDetectAmpInfos->at(i).nFFtIdx;
		float fMainValue = p_fVals[nFFTIdx];

		if (nPreviousIdx > 0 && nFFTIdx - nPreviousIdx > 5) {
			float fThreshold = 1; // nGroupCnt;
			// if (nGroupCnt > 3)
			// fThreshold = nGroupCnt * 2.0f / 3;
			if (nGroupMatchedCnt < fThreshold && !bMachineSound)
				return -1;
			nGroupCnt = 0;
			nGroupMatchedCnt = 0;

			nGroupIdx++;
		}

		if (fMainValue > (m_fGroupAvgAmps[nGroupIdx] * RELATED_RATE)
			&& fMainValue > p_fThreshold) 
		{
				nMatchedCnt++;
				nGroupMatchedCnt++;
		} 
		else 
		{
			if (p_lstDetectAmpInfos->at(i).fAmpVal >= 0.9f)
				return -1;
		}

		nGroupCnt++;

		nPreviousIdx = nFFTIdx;
	}

	if (nGroupCnt > 0) {
		float fThreshold = (float)nGroupCnt;
		// if (nGroupCnt > 3)
		// fThreshold = nGroupCnt * 2.0f / 3;
		if (nGroupMatchedCnt < fThreshold && !bMachineSound)
			return -1;
	}

	return nMatchedCnt;
}

bool CDetectMgr::IsRecording()
{
    return (m_pFileRecording != nullptr) && m_bRecording;
}

bool CDetectMgr::IsRecordingOrPreparing()
{
    return m_pFileRecording != nullptr;
}

void CDetectMgr::Recording(float* pVals)
{
    if (!m_bRecording || m_pFileRecording == nullptr || pVals == nullptr)
        return;
    
    fprintf(m_pFileRecording, "%d", m_nRecordingFrames);
    for (int i = minIdx; i <= maxIdx; i++)
    {
        if( pVals[i] >= 0 && m_pFileRecording != nullptr){
        fprintf(m_pFileRecording, "\t%f", pVals[i]);
        }
    }
    if (m_pFileRecording != nullptr) {
         fprintf(m_pFileRecording, "\n");
    }
    

}
void CDetectMgr::UpdateMatchThreshhold(float threshhold){
    for (int i=0;i<RST_COUNT;i++){
        MATCHING_RATE_THRESHOLD_DELTAS[i] = threshhold;
    }
}
bool CDetectMgr::Process(float* p_fData, int p_nFrameLen, int &p_nAlarmType, int &p_nAlarmIdx, int &soundToMatchWith)
{
	p_fData[0] = 0;

    printf("frame Len : %d And Alaram Type %d AAND Alaram Index :%d And Matching Sound : %d",p_nFrameLen,p_nAlarmType,p_nAlarmIdx,soundToMatchWith);
    printf("\n threshhold %d",valuesStr[soundToMatchWith].UNIVERSAL_THRESHOLDS);
	fft->forward(p_fData, FRAME_LEN);
	float* pVals = new float[maxIdx + 1];
	memset(pVals, 0x00, sizeof(float) * (maxIdx + 1));
    
    float fMaxVals[MAX_FREQ_CNT] = {0.0f};
    float fMaxFreqs[MAX_FREQ_CNT] = {0.0f};
    
    float val = 0.0f;
    float prevVal = 0.0f;
    float dist = 0.0f;
    float preDist = 0.0f;
    
    int idx = 0;
    
    if (m_pFreqIndicesToDraw == nullptr) {
        
        double freqStart = 20;
        double fFreqStep = pow(SAMPLE_FREQ / 2 / freqStart, 1.0 / (BAND_CNT - 1));
        double freq = freqStart;
        m_pFreqIndicesToDraw = new int[BAND_CNT];
        for (int i = 0; i < BAND_CNT; i++) {
            m_pFreqIndicesToDraw[i] = fft->freqToIndex((float)freq);
            freq = freq * fFreqStep;
        }
    }
    
    if (m_pRealValsToDraw == nullptr) {
        m_pRealValsToDraw = new float[BAND_CNT];
    }
    for (int i = 0; i < BAND_CNT; i++) {
        if (m_pFreqIndicesToDraw != nullptr){
        m_pRealValsToDraw[i] = (log10(fft->getBand(m_pFreqIndicesToDraw[i])) + 2) * 10;
        if (m_pRealValsToDraw[i] < 0)
            m_pRealValsToDraw[i] = 0;
        }
    }
    
    for (int i = 0; i < MAX_FREQ_CNT; i++)
    {
        fMaxVals[i] = 0.0f;
        fMaxFreqs[i] = 0.0f;
    }
    
    for (int i = minIdx; i <= maxIdx; i++)
    {
        val = fft->getBand(i);
        
        pVals[i] = val;
        
        dist = val - prevVal;
        if (preDist > 0 && dist < 0)
        {
            if (prevVal > fDeltaThreshold)
            {
                idx = 0;
                for (; idx < MAX_FREQ_CNT; idx++)
                {
                    if (fMaxVals[idx]==0 || prevVal > fMaxVals[idx])
                        break;
                }
                
                float fFreq = fft->indexToFreq(i - 1);
                if (idx < MAX_FREQ_CNT)
                {
                    for (int j = MAX_FREQ_CNT - 1; j > idx; j--)
                    {
                        if (fMaxVals[j-1] == 0)
                            continue;
                        fMaxVals[j] = fMaxVals[j-1];
                        fMaxFreqs[j] = fMaxFreqs[j-1];
                    }
                    fMaxVals[idx] = prevVal;
                    fMaxFreqs[idx] = fFreq;
                }
            }
        }
        
        preDist = dist;
        prevVal = val;
    }
    
    val = log10f(fMaxVals[0]);
    
	m_vecFftVals.push_back(pVals);
    
    if (m_vecFftVals.size() > MAX_MATCH_FRAME_CNT)
	{
		float* pVals1 = m_vecFftVals.at(0);
        if (pVals1 != nullptr){
        printf("ok");
		delete[] pVals1;
            m_vecFftVals.erase(m_vecFftVals.begin());
            pVals1 = nullptr;
        }
		
	}
    
    if (m_pFileRecording != nullptr && !m_bRecording && val > RECORD_START_THRESHOLD)
    {
        m_bRecording = true;
    }
    
    if (IsRecording())
    {
        Recording(pVals);
        m_nRecordingFrames += FRAME_LEN;
        if (m_lpFuncRecording != nullptr)
        {
            m_lpFuncRecording(m_pObj, RECORDING_PROGRESS, m_nRecordingFrames);
        }
        
        if (m_nRecordingFrames > MAX_RECORD_FRAMES[g_recordSoundType]*4)
        {
            while (m_vecFftVals.size() > 0)
            {
                float* pVals1 = m_vecFftVals.at(0);
                delete[] pVals1;
                m_vecFftVals.erase(m_vecFftVals.begin());
            }
            
            RecordStop();
        }
        return false;
    }

    if (g_bDetected)
        return false;
    
	bool bDetected = false;

	if (m_nDetectedFrames > 0) 
	{
		m_nDetectedFrames--;
		return bDetected;
	}
    
    NSLog(@"bDetected=%d         maxFreq=%f           maxVal=%f          Valid_Frames=%d      Repeat_Frames=%d     Invalid_Frames=%d\n\n\n",
          bDetected ? 1 : 0, fMaxFreqs[0], fMaxVals[0], Valid_Frames, Repeat_Frames, Invalid_Frames);
    if (bDetected){
        NSLog(@"hello");
    }
    int nUniversalThresholds = valuesStr[soundToMatchWith].UNIVERSAL_THRESHOLDS;

 
    if (appdelegate.maxFrequncyOfVoice < fMaxVals[0]) {
        appdelegate.maxFrequncyOfVoice = (float)fMaxVals[0];
    }
    
    
    if (appdelegate.maxFrequncyOfVoice > 100) {
        NSLog(@"maxval= %f  Fixed value = 100",appdelegate.maxFrequncyOfVoice);
        appdelegate.isMaxFrequncy = true;
        appdelegate.showAlertForFrequncy = true;
        // 1) (maxvalue > 100) then we ask the user (Please don't raise your voice) with “OK” button.
        
//    }else if(appdelegate.maxFrequncyOfVoice > valuesStr[soundToMatchWith].UNIVERSAL_MAX_FREQ && appdelegate.maxFrequncyOfVoice < 100){
    }else if(appdelegate.maxFrequncyOfVoice > 5 && appdelegate.maxFrequncyOfVoice < 100){
        // 3)  (maxvalue > UNIVERSAL_MAX_FREQ && maxvalue < 100) then this means everything is good
        appdelegate.isMaxFrequncy = false;
        appdelegate.showAlertForFrequncy = false;
    //}else if(appdelegate.maxFrequncyOfVoice < valuesStr[soundToMatchWith].UNIVERSAL_MAX_FREQ){
    }else if(appdelegate.maxFrequncyOfVoice < 5){
        NSLog(@"maxval= %f  UNIVERSAL_MAX_FREQ = %d",appdelegate.maxFrequncyOfVoice, valuesStr[soundToMatchWith].UNIVERSAL_MAX_FREQ);
        appdelegate.isMaxFrequncy = false;
        appdelegate.showAlertForFrequncy = true;
        // 2) (maxvalue < UNIVERSAL_MAX_FREQ) then we ask the user  (Please raise your voice) with “OK” button
        
    }
    
    if(!bDetected){

        if (fMaxFreqs[0] >= valuesStr[soundToMatchWith].UNIVERSAL_MIN_FREQ && fMaxFreqs[0] <= valuesStr[soundToMatchWith].UNIVERSAL_MAX_FREQ && fMaxVals[0] >= nUniversalThresholds)
        {
            Invalid_Frames = 0;
            Valid_Frames++;
            
            
            if (Valid_Frames == valuesStr[soundToMatchWith].UNIVERSAL_MIN_PERIOD_FRAMES)
            {
                Repeat_Frames++;
                
                if (Repeat_Frames >= valuesStr[soundToMatchWith].UNIVERSAL_MIN_REPEATS)
                {
                    bDetected = true;
                    p_nAlarmType = -1;
                    p_nAlarmIdx = 0;
                }
            }
            
        } else {
            
            Invalid_Frames++;
            
            if (Valid_Frames > valuesStr[soundToMatchWith].UNIVERSAL_MAX_PERIOD_FRAMES)
            {
                Valid_Frames = 0;
            }
            
            if (Valid_Frames > valuesStr[soundToMatchWith].UNIVERSAL_MAX_PERIOD_FRAMES && Repeat_Frames > 0)
            {
                Repeat_Frames = 0;
            }
        }
    }
    
	if (bDetected)
	{
        alreadyDetected = true;
        ResetFrameInfo();
		m_nDetectedFrames = 15;
        
        while (m_vecFftVals.size() > 0)
        {
            float* pVals1 = m_vecFftVals.at(0);
            delete[] pVals1;
            m_vecFftVals.erase(m_vecFftVals.begin());
        }
	}
	
	return bDetected;
}

void CDetectMgr::RecordButtonPressing(bool pressing){
    printf("\npressing::::%d\n",pressing);
    recordingContinue = pressing;
}

bool CDetectMgr::StopPressingAndSoundDetected(){
    if(!recordingContinue && alreadyDetected){
        alreadyDetected = false;
        return true;
    }
    
    return false;
}
