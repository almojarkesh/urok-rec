#pragma once

#include "FFT.h"
#include <vector>
#include "DetectingData.h"

typedef void (*LPFUNC_RECORDING)(void*, int, int);

class CDetectMgr
{
public:
    CDetectMgr(int p_nSamplingFreq);
    ~CDetectMgr(void);
    
    //record button issue //jml
    bool recordingContinue;
    bool alreadyDetected;
    void RecordButtonPressing(bool pressing);
    bool StopPressingAndSoundDetected();
    
    //
    void RecordStart(const char* lpszFilePath, LPFUNC_RECORDING lpFuncRecording, void* p_obj);
    void RecordStop();
    bool IsRecording();
    void StopRecording(const char* lpszFilePath,LPFUNC_RECORDING lpFuncRecording); // stop recording without savingData
    bool IsRecordingOrPreparing();
    
    bool Process(float* p_fData, int p_nFrameLen, int &p_nAlarmType, int &p_nAlarmIdx, int &soundToMatchWith);
    void ClearFftValues();
    
    void SetDetectSmokeAlarmOnly(bool bEnable);
    void UpdateMatchThreshhold(float threshhold);
private:
    
    bool CheckMatched(CDetectingData* p_DetectData, bool bMachineSound, float p_fMatchThreshold = -1.0f);
    
    int GetMatchedCount(float* p_fVals, std::vector<AmpInfo>* p_lstDetectAmpInfos, float p_fThreshold, bool bMachineSound);
    
    void Recording(float* pVals);
    
    void ResetFrameInfo();
private:
    //    AppDelegate *delegate;
    FFT* fft;
    std::vector<float*> m_vecFftVals;
    
    int minIdx;
    int maxIdx;
    bool m_bDetectSmokeAlarmOnly;
    
    int m_nDetectedFrames;
    
    float* m_fGroupAvgAmps;
    
    bool m_bRecording;
    FILE* m_pFileRecording;
    int m_nRecordingFrames;
    
    LPFUNC_RECORDING m_lpFuncRecording;
    void* m_pObj;
    
    /** Universal Engine Varaibles */
    int Valid_Frames;
    int Invalid_Frames;
    int Repeat_Frames;
    
    /** For drawing graph */
    int* m_pFreqIndicesToDraw;
    float* m_pRealValsToDraw;
};
