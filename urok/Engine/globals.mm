#include "globals.h"
#include "DetectMgr.h"
#include "IAudioDriver.h"

float MATCH_RATE_THRESHOLD = 0.90f;
int MAX_RECORD_FILECNT = 51;

RecordSoundType g_recordSoundType;
DoorbellType g_doorbellType;

std::vector<CDetectingData*> g_DetectData[RST_COUNT];



const int MAX_RECORD_FRAMES[RST_COUNT] = {
    40960 * 5 / 10, /* Doorbell Max Frames */
    40960 * 5 / 10, /* BackDoorbell Max Frames */
    40960 * 5 / 10, /* Smoke Max Frames */
    40960 * 5 / 10, /* Telephone Max Frames */
    40960 * 5 / 10, /* Intercom Max Frames */
    40960 * 5 / 10, /* CO2 Max Frames */
    40960 * 5 / 10, /* AlarmClock Max Frames */
    40960 * 5 / 10, /* Theft Max Frames */
    40960 * 5 / 10, /* Microwave Max Frames */
    40960 * 4 / 10,/*AmplicomDoorbell*/
    40960 * 4 / 10,/*Bellman AlarmClock*/
    40960 * 4 / 10,/*Bellman AlarmClock*/
    40960 * 4 / 10,/*Bellman AlarmClock*/
    40960 * 4 / 10,/*Bellman Baby Crying*/
    40960 * 4 / 10,/*Bellman Doorbell*/
    40960 * 4 / 10,/*Bellman Smoke Alarm*/
    40960 * 6 / 10, /* Telephone Max Frames */
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Byron Doorbell*/
    40960 * 4 / 10,/*Echo AlarmClock*/
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 4 / 10, /*Echo Doorbell  */
    40960 * 6 / 10, /* Echo Telephone Max Frames */
    40960 * 4 / 10, /*FriedLandEvo Doorbell  */
    40960 * 4 / 10, /*FriedLandEvo Doorbell  */
    40960 * 4 / 10, /*FriedLandEvo Doorbell  */
    40960 * 4 / 10, /*FriedLandEvo Doorbell  */
    40960 * 4 / 10, /*FriedLandEvo Doorbell  */
    40960 * 4 / 10, /*FriedLandEvo Doorbell  */
    
    40960 * 4 / 10, /*FriedLandLibra Doorbell  */
    40960 * 4 / 10, /*FriedLandLibra Doorbell  */
    40960 * 4 / 10, /*FriedLandLibra Doorbell  */
    40960 * 4 / 10, /*FriedLandLibra Doorbell  */
    40960 * 4 / 10, /*FriedLandLibra Doorbell  */
    40960 * 4 / 10, /*FriedLandLibra Doorbell  */
    40960 * 6 / 10, /* FriedLandLibra Telephone Max Frames */
    40960 * 4 / 10, /*FriedLandPlugin Doorbell  */
    40960 * 4 / 10, /*FriedLandPlugin Doorbell  */
    40960 * 4 / 10, /*FriedLandPlugin Doorbell  */
    40960 * 4 / 10, /*FriedLandPlugin Doorbell  */
    40960 * 4 / 10, /*FriedLandPlugin Doorbell  */
    40960 * 4 / 10, /*FriedLandPlugin Doorbell  */
    40960 * 6 / 10, /* GreenMarkAmplicall Telephone Max Frames */
    40960 * 6 / 10, /* GreenMarkCL1 Telephone Max Frames */
    40960 * 6 / 10, /* GreenMarkCL2 Telephone Max Frames */
    40960 * 4 / 10, /*GreenBrook Doorbell  */
    40960 * 4 / 10, /*GreenBrook Doorbell  */
    40960 * 4 / 10, /*GreenBrook Doorbell  */
};


//const char* g_strDoorbellTypes[DT_COUNT] = {"General", "DingDong", "MetalBell",
//    "Buzzer", "Melody"};

const char* PEBBLE_UUID = "69721c7e-69ef-40eb-a687-5649124ab141";


const float RECORD_START_THRESHOLD = 1.0f;

bool g_isEngineTerminated = true;
bool g_bDetecting = false;
bool g_bDetected = false;



int UNIVERSAL_THRESHOLD_DELTA = 0;
float MATCHING_RATE_THRESHOLD_DELTAS[RST_COUNT] = {0};

float RECORDED_SOUND_THRESHOLD[RST_COUNT] =
{
    
    
    /* P */ 0.80f,
    /* B */ 0.73f,
    /* T and Ed */ 0.74f,
    /* D and Ed */ 0.75f,
    /* K and C and Ck */ 0.70f,
    /* G */ 0.80f,  // NO
    /* J  and Dge*/ 0.72f,
    /* K and C and Ck */ 0.70f,
    /* S soft */ 0.53f,
    /* F and Ph */ 0.70f,
    /* F and Ph */ 0.70f,
    /* F and Ph */ 0.70f,
    /* G */ 0.80f,  // NO
    /* V */ 0.84f,
    /* Th noise */ 0.85f,
    /* Th soft */ 0.85f,
    /* S soft */ 0.53f,
    /*  Z and S and X */ 0.80f,
    /*  Z and S and X */ 0.80f,
    /* Sh and Ch */ 0.53f,
    /* Ch and tch*/ 0.70f,
    /* K And C Hard */ 0.70f,
    /* Sh and Ch */ 0.53f,
    /* J  and Dge*/ 0.72f,
    /* L and Tle */ 0.70f,
    /* R and Wr */ 0.70f,
    /* R and Wr */ 0.70f,
    /* W */ 0.70f,
    /* H */ 0.70f,
    /* Wh */ 0.70f,
    /* M */ 0.80f,
    /* N */ 0.80f,
    /* N and Knd*/ 0.80f,
    /* Ng */ 0.80f,
    /* Nk */ 0.80f,
    /* Qu  */ 0.64f,
    /* Y consonant UROK */ 0.0f,
    /* Ks */ 0.50f,
    /*  Z and S and X */ 0.80f,
    /* Gz */ 0.50f, // No
    /* K and C and Ck */ 0.70f,
    /* Ch */ 0.70f,
    /* J  and Dge*/ 0.72f,
    
    /* Vowels */
    
    /* I short and Y */ 0.70f, // somtimes
    /* I long UROK */ 0.0f,
    /* E short */ 0.70f,
    /* E long UROK */ 0.0f,
    /* A short */ 0.65f,
    /* A long UROK */ 0.0f,
    /* U short */ 0.70f,
    /* U long UROK */ 0.0f,
    /* O short */ 0.70f,
    /* O long UROK */ 0.0f,
    /* I short and Y */ 0.70f, // somtimes
    /* I long UROK */ 0.0f,
    /* E long UROK */ 0.0f,
    
    /* Consonant -le */
    
    /* ble UROK */ 0.0f,
    /* dle UROK */ 0.0f,
    /* Ple UROK */ 0.0f,
    /* tle UROK */ 0.0f,
    /* L and TLE UROK */ 0.70f,
    /* gle UROK */ 0.0f,
    /* fle UROK */ 0.0f,
    /* kle UROK */ 0.0f,
    /* cle UROK */ 0.0f,
    /* zle UROK */ 0.0f,
    
    /* Control R's */
    
    /* Or and Ar UROK */ 0.0f,
    /* Er and Ur and Ir and Or and Ar UROK */ 0.0f,
    /* Er and Ur and Ir and Or and Ar UROK */ 0.0f,
    /* Er and Ur and Ir and Or and Ar UROK */ 0.0f,
    /* Er and Ur and Ir and Or and Ar UROK */ 0.0f,
    /* Ar UROK */ 0.0f,
    /* Er and Ur and Ir and Or and Ar UROK */ 0.0f,
    /* Or and Ar UROK */ 0.0f,
    
    /* Diphthongs */
    
    /* Oy and Oi UROK */ 0.0f,
    /* Oy and Oi UROK */ 0.0f,
    /* Au and Aw */ 0.65f,
    /* Au and Aw */ 0.65f,
    /* Ou and Ow */ 0.70f,
    /* Oo and Ou and UI and U_e and Ew */ 0.70f,
    /* Ou and Ow */ 0.70f,
    /* O long and Ow UROK */ 0.0f,
    /* Oo */ 0.75f,
    /* Oo and Ou and UI and U_e and Ew */ 0.70f,
    /* Oo and Ou and UI and U_e and Ew */ 0.70f,
    /* U Long UROK */ 0.0f,
    /* Oo and Ou and UI and U_e and Ew */ 0.70f,
    /* Oo and Ou and UI and U_e and Ew */ 0.70f,
    /* U long UROK */ 0.0f,
    /* Oo and Ou and UI and U_e and Ew */ 0.70f,
    /* U long UROK */ 0.0f,
    
    /* Word Beginnings & Endings */
    
    /* pre UROK */ 0.0f,
    /* be UROK */ 0.0f,
    /* un UROK */ 0.0f,
    /* dis UROK */ 0.0f,
    /* under UROK */ 0.0f,
    /* sub UROK */ 0.0f,
    /* in UROK */ 0.0f,
    /* mis UROK */ 0.0f,
    /* out UROK */ 0.0f,
    /* fore UROK */ 0.0f,
    /* de UROK */ 0.0f,
    /* re UROK */ 0.0f,
    /* ly UROK */ 0.0f,
    /* less UROK */ 0.0f,
    /* ment UROK */ 0.0f,
    /* ful UROK */ 0.0f,
    /* ness UROK */ 0.0f,
    /* en UROK */ 0.0f,
    /* ing UROK */ 0.0f,
    /* est UROK */ 0.0f,
    /* able UROK */ 0.0f,
    /* age UROK */ 0.0f,
    /* ed UROK */ 0.0f,
    /* D and Ed */ 0.75f,
    /* D and Ed */ 0.74f,
    /* tion UROK */ 0.0f,
    /* tion UROK */ 0.0f,
    /* sion UROK */ 0.0f,
    /* ture UROK */ 0.0f,
    /* ous UROK */ 0.0f,
    /* es UROK */ 0.0f,
    
    
};
bool IS_BACKGROUND = false;
CDetectMgr* g_pDetectMgr;
AudioParameterInfo* g_audioInfo = nullptr;

std::vector<std::string> split(std::string str, std::string sep)
{
    char* cstr=const_cast<char*>(str.c_str());
    char* current;
    std::vector<std::string> arr;
    current=strtok(cstr, sep.c_str());
    while(current!=NULL){
        arr.push_back(current);
        current=strtok(NULL,sep.c_str());
    }
    
    return arr;
}

RWBuffer	g_RecOutBuffer;
float*	g_fBufData;


int audioCallback(const void* inData, void* outData, unsigned long numSamples) {
    
    //long samples = numSamples;
    
    
    float **in = (float **)inData;
    
    if (g_RecOutBuffer.GetWriteSpace() < (int)(numSamples * sizeof(float)))
    {
        //... 록음자료를 출력할 빈 령역이 부족한 상태
    }
    else
    {
        
        g_RecOutBuffer.WriteData(in[0], (int)numSamples*sizeof(float));
    }
    
    return 0;
}

char g_strSoundObjectID[1024] = {0};

