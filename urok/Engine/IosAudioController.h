//
//  IosAudioController.h
//  EasyPlayer
//
//  Created by ezSampler on 12. 9. 18..
//  Copyright (c) 2012년 ezSampler. All rights reserved.
//

#ifndef __IosAudioController__
#define __IosAudioController__

#include "IAudioDriver.h"

class IosAudioController : public IAudioDriver {
public:
    static IosAudioController* getInstance();
    
    virtual ~IosAudioController()   {   close();    }
    
    virtual int  open(const AudioParameterInfo* paramInfo, AudioCallback callback);
	virtual void close();
    
    void play();
    void pause();
    
	virtual bool isOpened();
    
private:
    IosAudioController()    {}
};

#endif /* defined(__IosAudioController__) */
