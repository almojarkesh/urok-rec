//
//  SoundTestViewController.m
//  urok
//
//  Created by Muhammad Ahsan on 6/16/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import "SoundTestViewController.h"
#import "AudioController.h"
#import "SpeechRecognitionService.h"
#import "google/cloud/speech/v1/CloudSpeech.pbrpc.h"
#import "SoundUtil.h"
#import "EngineUtil.h"
#import "AppDelegate.h"
#import <Speech/Speech.h>

//#define SOCOREOBJECTKEY @"databaseValue"
#define SOCOREOBJECTKEY @"databaseValue"
#define IS_TIMER_ENABLE 0
#define TOTAL_TIME 1200
#define SOUND_TIME 5.0
#define SAMPLE_RATE 16000.0f
#define Google_API_KEY @"AIzaSyBlfaWbaKLUI4URbzmGEdw4G85VAOhic8s"

@interface SoundTestViewController ()<AudioControllerDelegate, AVAudioPlayerDelegate, AVSpeechSynthesizerDelegate,SFSpeechRecognizerDelegate,SFSpeechRecognitionTaskDelegate>{
    AVAudioPlayer *_audioPlayer;
    CADisplayLink *playProgressDisplayLink;
    BOOL isRecording;
    BOOL isPressing;
    BOOL compared;
    
    // new variables
    SFSpeechAudioBufferRecognitionRequest *request2;
    SFSpeechRecognitionTask *currentTask;
    AVAudioEngine *audioEngine;
    BOOL soundMessage;
    SFSpeechRecognizer *recognizer;
    NSTimer * timerToStop;
    
}
#define PressCompare @"Press and hold Speak button and say the following letter:"
#define ComparisonDone @"processing done please click next"
#define Recording @"Please say the following letter"
@property (nonatomic, strong) NSMutableData *audioData;
@property (nonatomic, retain) NSMutableArray* phonemeArray;
@property (nonatomic, retain) NSMutableArray* arrServerAudio;
@property (nonatomic, retain) NSString* soundObject;
@property (nonatomic, retain) NSMutableDictionary* scoreDic;
@property (assign) NSUInteger soundsCount;
@property (assign) BOOL isRecognised;
@property (assign) BOOL isTestComplete;

@property (weak, nonatomic) IBOutlet UIImageView *animatingImageView;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *soudTimerProgressbar;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *fullTimerProgressbar;

@property (weak, nonatomic) IBOutlet UILabel *SoundLabel;

@property (weak, nonatomic) IBOutlet UILabel *soundCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *fullTimeCountLabel;

@property (nonatomic, retain) MZTimerLabel *soundObjectTimer;
@property (nonatomic, retain) MZTimerLabel *fullTestTimer;

@property (nonatomic, retain) CABasicAnimation *animation;
@property (weak, nonatomic) IBOutlet UILabel *responseLabel;
@property (nonatomic, retain) NSError *recordingError;
@property (nonatomic, retain) AVSpeechSynthesizer *synthesizer;

@property (assign) NSUInteger timesRequest;
@property (assign) NSUInteger timesResponse;
@property (weak, nonatomic) IBOutlet UIImageView *equlizerImageView;
@property (nonatomic, retain) NSString* testNumber;
@property (retain, nonatomic) IBOutlet UIButton *listenButton;

@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) AVAudioRecorder *audioToUpload;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *progressStop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *soundViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *speakButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stopProgressHeight;

@end
NSString *IF_DETECTED_OBSERVER = @"DetectedSound";

@implementation SoundTestViewController
int indexOfSound = 0;

int compareForTheFirstTime = 0;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _responseLabel.numberOfLines = 0;
    _responseLabel.textAlignment=NSTextAlignmentCenter;
    //    _responseLabel.adjustsFontSizeToFitWidth = true;
    [self addSoundFile];
    [self.view endEditing:true];
    indexOfSound = 120;
    _audioPlayer.delegate = self;
    
    //  compared = false;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(startAgain) name:@"StartAgain" object:nil];
    _arrServerAudio = [[NSMutableArray alloc]init]; // this gets all the sounds
    
    //    self.frameHeightConstraint.constant = backgroundImage.size.height;
    
    
    //UI Setup for iPad
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.soundViewHeight.constant = 200;
        self.speakButtonHeight.constant = 300;
        self.tblWidth.constant = 150;
        self.stopProgressHeight.constant = 250;
    }
    
   
    
}
-(void)startAgain{
    indexOfSound = 0;
    //    [self nextAction:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"SoundTestViewController");
    [super viewWillAppear:animated];
    appdelegate.showAlertForFrequncy = false;
    //    recognizer = [[SFSpeechRecognizer alloc] initWithLocale:[NSLocale localeWithLocaleIdentifier:@"en-US"]];
    //
    //    [recognizer setDelegate:self];
    //    [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus authStatus) {
    //        switch (authStatus) {
    //            case SFSpeechRecognizerAuthorizationStatusAuthorized:
    //                //User gave access to speech recognition
    //                NSLog(@"Authorized");
    //                break;
    //
    //            case SFSpeechRecognizerAuthorizationStatusDenied:
    //                //User denied access to speech recognition
    //                NSLog(@"SFSpeechRecognizerAuthorizationStatusDenied");
    //                break;
    //
    //            case SFSpeechRecognizerAuthorizationStatusRestricted:
    //                //Speech recognition restricted on this device
    //                NSLog(@"SFSpeechRecognizerAuthorizationStatusRestricted");
    //                break;
    //
    //            case SFSpeechRecognizerAuthorizationStatusNotDetermined:
    //                //Speech recognition not yet authorized
    //
    //                break;
    //
    //            default:
    //                NSLog(@"Default");
    //                break;
    //        }
    //    }];
    
    
    _isTestComplete = false;
    _equlizerImageView.image = nil;
    _timesRequest = 0.0;
    _timesResponse = 0.0;
    [[EngineUtil shared] deinitEngine];
    [[EngineUtil shared]initEngine]; // for test sound
    [[EngineUtil shared]updateMatchingThreshhold:0.0];
    indexOfSound = 0;
    
    [AudioController sharedInstance].delegate = self;
    _phonemeArray = [[SoundUtil sharedInstance] getSounds:false]; // this gets all the sounds
    _soundsCount = _phonemeArray.count;
    _scoreDic = [[NSMutableDictionary alloc]init];
    //    _soudTimerProgressbar.value = 0.0;
    
    
    _synthesizer = [[AVSpeechSynthesizer alloc] init];
    _synthesizer.delegate = self;
    
    NSDictionary<NSString*, NSString *> *testObject = @{@"dot" : [NSDate date]};
    id<IDataStore>dataStore = [backendless.data ofTable:@"ScoringResultDetail"];// removed @"user_id" : [[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"],
    [SVProgressHUD showWithStatus:@"Starting Test..."];
    [dataStore save:testObject response:^(id response) {
        self.testNumber = [response valueForKey:@"objectId"];
        [self nextAction:self];
        [SVProgressHUD dismiss];
        
        
        if(IS_TIMER_ENABLE){
            _fullTimerProgressbar.hidden = FALSE;
            _fullTestTimer = [[MZTimerLabel alloc] initWithLabel:_fullTimeCountLabel andTimerType:MZTimerLabelTypeTimer];
            _fullTestTimer.timeLabel = _fullTimeCountLabel;
            _fullTestTimer.timerType = MZTimerLabelTypeTimer;
            [_fullTestTimer setCountDownTime:TOTAL_TIME +1];
            _fullTestTimer.timeFormat = @"mm:ss";
            _fullTimerProgressbar.value = 0.0;
            [_fullTestTimer startWithEndingBlock:^(NSTimeInterval countTime) {
                [self testCompletAction];
            }];
            [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkForTime) userInfo:nil repeats:NO];
        }
    } error:^(Fault *fault) {
        [SVProgressHUD dismiss];
    }];
    
    
    compared = false;
    
    [self checkSwitch];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    _isTestComplete = true;
}

-(void)checkSwitch{
    _listenButton.hidden = true;
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"listen_button_enable"]){
        _listenButton.hidden = false;
    }
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance{
    
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menuAction:(id)sender {
    MenuView *menu = [[MenuView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [menu show];
    [menu setSwitchChange:^{
        [self checkSwitch];
    }];
}

- (IBAction)recordAudio:(id)sender {
    
    //    if(backendles] == 1){
    
    //RecordAudio
    appdelegate.maxFrequncyOfVoice = 0.0;
    NSLog(@"***** IEFT Touch down is called ******");
    
    _responseLabel.text = PressCompare;
    
    //    if ([[_soundObject valueForKey:@"type"] isEqualToString : UROKSOUND]){
    //
    //        [_synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    //        [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    //
    //
    //        isRecording = true;
    //        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    //        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    //        [_audioRecorder record];
    //        NSLog(@"SoundTestViewController Recording : Done");
    //        _audioRecorder.meteringEnabled = true;
    //
    //        [playProgressDisplayLink invalidate];
    //        playProgressDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updatePlayProgress)];
    //        playProgressDisplayLink.frameInterval = 5; // 0.25s, assuming 60Hz display refresh rate
    //
    //        [playProgressDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    //    }
    //    else if ([[_soundObject valueForKey:@"type"] isEqualToString : SOUNDALERT])
    //    {
    
    [[SoundUtil sharedInstance] stopPlaying];
    [_synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:IF_DETECTED_OBSERVER object:nil];
    isRecording = true;
    NSLog(@"Record button for sound Alert, Touch Down");
    [SoundUtil sharedInstance].recorder.meteringEnabled = true;
    [[EngineUtil shared] startRecording];
    //[self btnRecordAction:nil];
    [self btnCompareAction:nil];
    self.progressStop.value = 0;
       timerToStop = [NSTimer scheduledTimerWithTimeInterval: 0.05 target: self selector:@selector(increaseProgress) userInfo: nil repeats:YES];
    
    //    }
    //    else
    //    {
    //        [[SoundUtil sharedInstance] stopPlaying];
    //
    //        [_synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    //        [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    //
    //        [[NSNotificationCenter defaultCenter]removeObserver:self name:IF_DETECTED_OBSERVER object:nil];
    //        isRecording = true;
    //        NSLog(@"Record button for sound Alert, Touch Down");
    //        [SoundUtil sharedInstance].recorder.meteringEnabled = true;
    //        //        [[EngineUtil shared] startRecording];
    //        //[self btnRecordAction:nil];
    //        [self btnCompareAction1:nil];
    //        [self startRecording];
    //    }
    [self initSoundToUpload];
    //    }else{
    //        ALERT_VIEW(@"No Internet", @"You will be not able to continue until you can establish an Internet Connection.");
    //    }
    
}
-(void)increaseProgress{
    NSLog(@"timer %f",self.progressStop.value);
    self.progressStop.value = self.progressStop.value + 0.05;
    if(self.progressStop.value >= 1.0){
        [timerToStop invalidate];
        if(timerToStop){
            timerToStop = nil;
        }
        [self stopAudio:nil];
    }
}

- (IBAction)stopAudio:(id)sender {
    //touchupinside
    //RecordAudio
    
   
    
    NSLog(@"***** IEFT Touch up inside is called ******");
    //    if ([[_soundObject valueForKey:@"type"] isEqualToString : UROKSOUND]){
    //
    //        isRecording = false;
    //
    //        if (_audioRecorder.recording) {
    //            [_audioRecorder stop];
    //        } else if (_audioPlayer.playing) {
    //            [_audioPlayer stop];
    //        }
    //        //_responseLabel.text = @"Stopped Recording and waiting for Response";
    //
    //        [playProgressDisplayLink invalidate];
    //        playProgressDisplayLink = nil;
    //        [ self.animatingImageView.layer removeAllAnimations];
    //        _equlizerImageView.image = nil;
    //        [self processAudio:self]; // the google api
    //
    //    }
    //    else if ([[_soundObject valueForKey:@"type"] isEqualToString : SOUNDALERT]){
    
    isRecording = false;
    [[EngineUtil shared] stopRecording];
    [[EngineUtil shared] stopDetecting];
    [[EngineUtil shared] stopDetecting];
    [self stopEqualizer];
    [self performSelector:@selector(stopComparison) withObject:nil afterDelay:0.5];
    //    }
    //
    //    else{
    //        isRecording = false;
    //        [[EngineUtil shared] stopRecording];
    //        [[EngineUtil shared] stopDetecting];
    //        [self stopEqualizer];
    //        //        [self performSelector:@selector(stopComparison) withObject:nil afterDelay:0.5];
    //        //_responseLabel.text = @"Stopped Recording and waiting for Response";
    //
    //        [[EngineUtil shared]stopDetecting];
    //        [self stopEqualizer];
    //        [audioEngine stop];
    //        [request2 endAudio];
    //        //[self stopComparison];
    //    }
    _responseLabel.text = ComparisonDone;
    [self stopToRecordForServer];
    self.progressStop.value = 1.0;
    [timerToStop invalidate];
    if(timerToStop){
        timerToStop = nil;
    }
    if (appdelegate.showAlertForFrequncy) {
        if (appdelegate.isMaxFrequncy) {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Please lower your voice"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yes = [UIAlertAction
                                  actionWithTitle:@"Ok"
                                  style:UIAlertActionStyleCancel
                                  handler:^(UIAlertAction * action){
                                  }];
            [alert addAction:yes];
            [appdelegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
        }else{
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Please raise your voice"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yes = [UIAlertAction
                                  actionWithTitle:@"Ok"
                                  style:UIAlertActionStyleCancel
                                  handler:^(UIAlertAction * action){
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

//Sound to upload On Server
- (NSString *)soundUploadFilePath {
    //START: Initialize the Server Directory
    NSArray* w_pArrDirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* w_pStrSoundDetectDir = [(NSString*)[w_pArrDirs objectAtIndex:0] stringByAppendingPathComponent:@"Server"];
    
    NSError* error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:w_pStrSoundDetectDir])
    [[NSFileManager defaultManager] createDirectoryAtPath:w_pStrSoundDetectDir withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    //END: server initialization
    
    NSString *strDatabaseName = [[_phonemeArray objectAtIndex:indexOfSound-1]valueForKey:@"databaseValue"];
    NSString * fileName = [NSString stringWithFormat:@"%@-%.0f.wav",strDatabaseName,[[NSDate date] timeIntervalSince1970] * 1000];
    NSString* docsDir = [NSString stringWithFormat:@"%@/%@",w_pStrSoundDetectDir,fileName];
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    [dictTemp setObject:docsDir forKey:@"filePath"];
    [dictTemp setObject:fileName forKey:@"fileName"];
    [dictTemp setObject:@"0" forKey:@"isUploaded"];
    [dictTemp setObject:strDatabaseName forKey:@"folderName"];
    
    [_arrServerAudio addObject:dictTemp];
    return docsDir;
}

-(void)initSoundToUpload{
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:[self soundUploadFilePath]];
    
    NSDictionary *recordSettings = @{AVSampleRateKey:@44100.0,
                                     AVFormatIDKey:@(kAudioFormatLinearPCM),
                                     AVNumberOfChannelsKey:@2,
                                     AVEncoderBitRateKey: @16,
                                     AVEncoderAudioQualityKey:@(AVAudioQualityMax),
                                     AVSampleRateKey: @(SAMPLE_RATE)
                                     };
    //AVFormatIDKey:@(kAudioFormatLinearPCM),
    
    NSLog(@"Server Path : %@",soundFileURL.absoluteString);
    NSError *error;
    _audioToUpload = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
    if (!error) {
        [_audioToUpload prepareToRecord];
        [_audioToUpload record];
    }
}
-(void)stopToRecordForServer{
    [_audioToUpload stop];
    if (!appdelegate.showAlertForFrequncy) {
        [self uploadSoundFileWithIndex:0];
        NSLog(@"Upload");
    }else{
        NSLog(@"Delete");
        if (_arrServerAudio.count > 0) {
            [_arrServerAudio removeLastObject];
        }
    }
}
-(void)uploadSoundFileWithIndex:(int)index{
    if ((index == 0 && _arrServerAudio.count > 0) || (_arrServerAudio.count > 0 && index < _arrServerAudio.count)) {
        NSMutableDictionary *objDict = [_arrServerAudio objectAtIndex:index];
        
        NSData *audioData = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@",objDict[@"filePath"]]];
        NSString *strServerpath = [NSString stringWithFormat:@"/media/%@/%@",objDict[@"folderName"],objDict[@"fileName"]];
        NSLog(@"strServerpath %@",strServerpath);
        if (audioData != nil) {
            [[Backendless sharedInstance].fileService saveFile:strServerpath content:audioData response:^(BackendlessFile * file) {
                NSLog(@"Uploaded successfully");
                [self removeFileFrom:objDict[@"filePath"]];
                if (_arrServerAudio.count > 0) {
                    [_arrServerAudio removeObjectAtIndex:0];
                    [self uploadSoundFileWithIndex:0];
                }
            } error:^(Fault *error) {
                NSLog(@"%@",error.description);
            }];
        }  
    }
}

- (void)removeFileFrom:(NSString *)filePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"File deleted");
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}
-(void)startRecording
{
    
    NSString* wordObject = [_soundObject valueForKey:@"word"];
    
    
    NSArray* optionsArray = [_soundObject valueForKey:@"options"];
    NSArray* matchArray = [_soundObject valueForKey:@"wordMatch"];
    
    audioEngine = [[AVAudioEngine alloc] init];
    //_responseLabel.text = @"Listening to compare...";
    
    // Make sure there's not a recognition task already running
    if (currentTask) {
        [currentTask cancel];
        currentTask = nil;
    }
    
    soundMessage = NO;
    
    // Starts an AVAudio Session
    NSError *error;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    //    [audioSession setCategory:AVAudioSessionCategoryRecord error:&error];
    
    //    [audioSession setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:&error];
    [audioSession setCategory:AVAudioSessionCategoryRecord withOptions: AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionAllowBluetoothA2DP | AVAudioSessionCategoryOptionAllowAirPlay error:&error];
    [audioSession setActive:YES error:nil];
    //    [_audioRecorder record];
    //    _audioRecorder.meteringEnabled = true;
    
    request2 = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
    
    AVAudioInputNode *inputNode =  audioEngine.inputNode;
    
    inputNode = [audioEngine inputNode];
    
    if (request2 == nil) {
        NSLog(@"Unable to created a SFSpeechAudioBufferRecognitionRequest object");
    }
    
    if (inputNode == nil) {
        NSLog(@"Unable to created a inputNode object");
    }
    
    request2.shouldReportPartialResults = true;
    _isRecognised = false;
    currentTask = [recognizer recognitionTaskWithRequest:request2 resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error)
                   {
                       
                       BOOL isFinal = NO;
                       if (result)
                       {
                           // Whatever you say idatan the microphone after pressing the button should be being logged in the console.
                           NSLog(@"result = %@",result);
                           
                           if(!_isRecognised)
                           {
                               [self btnCompareAction1:nil];
                               [[SoundUtil sharedInstance] stopRecorderAndOffset:0];
                               [self stopDetectionForNextSound];
                               [audioEngine stop];
                               [currentTask cancel];
                               [request2 endAudio];
                               request2 = nil;
                               currentTask = nil;
                               [audioEngine stop];
                               [request2 endAudio];
                               [self stopEqualizer];
                               [[EngineUtil shared] stopDetecting];
                               
                               
                               NSLog(@"RESULT:%@",result.bestTranscription.formattedString);
                               isFinal = !result.isFinal;
                               
                               for(NSString * word in optionsArray){
                                   if(_isRecognised)
                                   break;
                                   
                                   NSString* alternativeString = [[result.bestTranscription.formattedString lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
                                   if([alternativeString isEqualToString: [word lowercaseString]]){
                                       _isRecognised = true;
                                       [_scoreDic setValue:@(1) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
                                       if([wordObject isEqualToString:wordObject])
                                       {
                                           //_responseLabel.text = [NSString stringWithFormat:@"%@ %@",ComparisonDone,wordObject];
                                           //                                           [self playMessageSound:[NSString stringWithFormat:@"You said it correctly"]];//wordObject
                                           NSLog(@"You said it correctly1");
                                           [self btnCompareAction1:nil];
                                           soundMessage = YES;
                                           [audioEngine stop];
                                           [request2 endAudio];
                                           [self stopEqualizer];
                                           [[EngineUtil shared] stopDetecting];
                                           request2 = nil;
                                           currentTask = nil;
                                       }
                                       break;
                                   }
                                   
                                   if([wordObject.lowercaseString isEqualToString:@"sh"])
                                   {
                                       for(NSString * word1 in matchArray){
                                           if(_isRecognised)
                                           break;
                                           else if ([alternativeString.lowercaseString rangeOfString:word1.lowercaseString].location != NSNotFound)
                                           {
                                               _isRecognised = true;
                                               [_scoreDic setValue:@(1) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
                                               //_responseLabel.text = [NSString stringWithFormat:@"%@ %@",ComparisonDone,wordObject];
                                               //                                               [self playMessageSound:[NSString stringWithFormat:@"You said it correctly"]];
                                               NSLog(@"You said it correctly2");
                                               [self btnCompareAction1:nil];
                                               soundMessage = YES;
                                               [audioEngine stop];
                                               [request2 endAudio];
                                               [self stopEqualizer];
                                               [[EngineUtil shared] stopDetecting];
                                               request2 = nil;
                                               currentTask = nil;
                                               break;
                                           }
                                       }
                                   }
                               }
                               if (!_isRecognised)
                               {
                                   if (soundMessage == NO)
                                   {
                                       
                                       [self btnCompareAction1:nil];
                                       [audioEngine stop];
                                       [request2 endAudio];
                                       [self stopEqualizer];
                                       [[EngineUtil shared] stopDetecting];
                                       request2 = nil;
                                       currentTask = nil;
                                       //_responseLabel.text = [NSString stringWithFormat:@"Processing done and you did not say it correctly."];
                                       //                                       [self playMessageSound:[NSString stringWithFormat:@"Sorry! you did not speak it correctly."]];
                                       soundMessage = YES;
                                       
                                   }
                               }
                           }
                       }
                       if (error) {
                           if (!soundMessage)
                           {
                               [self btnCompareAction1:nil];
                               
                               [audioEngine stop];
                               [request2 endAudio];
                               [self stopEqualizer];
                               [[EngineUtil shared] stopDetecting];
                               request2 = nil;
                               currentTask = nil;
                               //_responseLabel.text = [NSString stringWithFormat:@"Did not detected any Speech. Try Again"];
                               //                               [self playMessageSound:[NSString stringWithFormat:@"Did not detected any Speech. Try Again"]];
                               soundMessage = YES;
                           }
                       }
                   }];
    AVAudioFormat *recordingFormat = [inputNode outputFormatForBus:0];
    
    [inputNode installTapOnBus:0 bufferSize:4096 format:recordingFormat block:^(AVAudioPCMBuffer *buffer, AVAudioTime *when){
        NSLog(@"Block tap!");
        [request2 appendAudioPCMBuffer:buffer];
    }];
    
    [audioEngine prepare];
    [audioEngine startAndReturnError:&error];
    NSLog(@"Error %@", error);
}


#pragma mark - Audio Play

-(void)updatePlayProgress{
    [_audioRecorder updateMeters];
    CGFloat normalizedValue = pow (10, [_audioRecorder averagePowerForChannel:0] / 20);
    _equlizerImageView.animationDuration = 1.0;
    _equlizerImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld",[self getAnimation:normalizedValue]]];
    
    
}
-(void)updatePlayProgressForTestSoundApp{
    [[SoundUtil sharedInstance].recorder updateMeters];
    
    CGFloat normalizedValue = pow (10, [[SoundUtil sharedInstance].recorder averagePowerForChannel:0] / 20);
    _equlizerImageView.animationDuration = 1.0;
    _equlizerImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld",[self getAnimation:normalizedValue]]];
    
}


-(void)checkForTime{
    if(!_isTestComplete){
        if(_soudTimerProgressbar.value != 100){
            float totalTime = SOUND_TIME;
            if([_soundCountLabel.text isEqualToString:@""]){
                _soudTimerProgressbar.value = 0.0;
            }else {
                _soudTimerProgressbar.value = 100-([_soundCountLabel.text integerValue]*100)/totalTime;
            }
        }
        else{
            _soudTimerProgressbar.value = 0.0;
        }
        if(_fullTimerProgressbar.value != 100){
            float totalTime = TOTAL_TIME;
            float timeValue = 0.0;
            int i = 0;
            for(NSString *time in [_fullTimeCountLabel.text componentsSeparatedByString:@":"]){
                if(i == 0){
                    timeValue = [time integerValue]*60;
                } else {
                    timeValue += [time integerValue];
                }
                i++;
            }
            
            _fullTimerProgressbar.value = 100-(timeValue*100)/totalTime;
        }
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkForTime) userInfo:nil repeats:NO];
    }
}

- (IBAction)nextAction:(id)sender {
    [self nextClick];
    //    indexOfSound++;
}

- (IBAction)nextClick {
    
    appdelegate.showAlertForFrequncy = false;
    compared = false;
    [self updateDetail];
    compareForTheFirstTime = 0;
    [self stopDetectionForNextSound]; // testSoundApp Engine turned off
    [_synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    //    [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    _responseLabel.text = PressCompare;
    [[AudioController sharedInstance] stop];
    [[SpeechRecognitionService sharedInstance] stopStreaming];
    [self.animatingImageView.layer removeAllAnimations];
    if(indexOfSound < _phonemeArray.count){
        if(IS_TIMER_ENABLE){
            [_soundObjectTimer reset];
            [_soundObjectTimer pause];
            _soudTimerProgressbar.hidden = false;
            //            [self checkForTime];
            //            [self performSelector:@selector(checkForTime) withObject:nil afterDelay:0.3];
            _soudTimerProgressbar.value = 0.0;
            _soundCountLabel.text = @"";
        }
        
        _soundObject = [_phonemeArray objectAtIndex:indexOfSound];
        _SoundLabel.text = [_soundObject valueForKey:@"word"];
        //        if ([[_soundObject valueForKey:@"type"] isEqualToString : UROKSOUND]){
        //            _isRecognised = NO;
        //
        //            [self listenAction:self];
        //            [_scoreDic setValue:@(2) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
        //
        //            //_responseLabel.text = @"";
        //        }
        //        else if  ([[_soundObject valueForKey:@"type"] isEqualToString : SOUNDALERT]){
        [self stopRecordingAndEquilizer];
        
        [SoundUtil sharedInstance].currentSound = self.phonemeArray[indexOfSound];
        [[SoundUtil sharedInstance]initializeRecorderForSoundId:indexOfSound andOffset:0];
        [self playSoundAtIndex:indexOfSound];
        _responseLabel.text = PressCompare; // prevously StartRecording
        [_scoreDic setValue:@(2) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
        
        //        }
        //        else if  ([[_soundObject valueForKey:@"type"] isEqualToString : SIRI]){
        //            [self stopRecordingAndEquilizer];
        //
        //            [SoundUtil sharedInstance].currentSound = self.phonemeArray[indexOfSound];
        //            [[SoundUtil sharedInstance]initializeRecorderForSoundId:indexOfSound andOffset:0];
        //            [self playSoundAtIndex:indexOfSound];
        //            _responseLabel.text = PressCompare; // prevously StartRecording
        //            [_scoreDic setValue:@(2) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
        //
        //        }
        indexOfSound++;
    }
    else {
        //        [self testCompletAction];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Do you need to start from beginning?"
                                      message:nil
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yes = [UIAlertAction
                              actionWithTitle:@"YES"
                              style:UIAlertActionStyleCancel
                              handler:^(UIAlertAction * action){
                                  indexOfSound = 0;
                                  [self nextClick];
                                  [self.tblData setContentOffset:CGPointZero animated:YES];
                              }];
        UIAlertAction* no = [UIAlertAction
                             actionWithTitle:@"NO"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action){
                                 
                             }];
        [alert addAction:yes];
        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

-(void)equalizerForCompariosn{
    
    
    
    [SoundUtil sharedInstance].recorder.meteringEnabled = true;
    
    [playProgressDisplayLink invalidate];
    playProgressDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updatePlayProgressForTestSoundApp)];
    playProgressDisplayLink.frameInterval = 5; // 0.25s, assuming 60Hz display refresh rate
    
    [playProgressDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    
    [[SoundUtil sharedInstance] startRecordingForEqualizer];
}
-(void)testCompletAction{
    _isTestComplete = true;
    
    //    NSArray * scoreArray = [_scoreDic allValues];
    //    int correctScore = 0;
    //    int wrongScore = 0;
    //    for(int i = 0; i< scoreArray.count ; i++){
    //        if ([[scoreArray objectAtIndex:i] intValue] == 1){
    //            correctScore++;
    //        } else {
    //            wrongScore++;
    //        }
    //    }
    
    //    ScoreViewController * contoller = [self.storyboard instantiateViewControllerWithIdentifier:@"ScoreViewController"];
    //    contoller.correctScore = [NSString stringWithFormat:@"%d", correctScore];
    //    contoller.wrongScore = [NSString stringWithFormat:@"%lu", _soundsCount - correctScore];
    //    contoller.testNumber = self.testNumber;
    //    [self.navigationController pushViewController:contoller animated:YES];
    //    [self updateDetail];
    if ([[EngineUtil shared] isDetecting]){
        [[EngineUtil shared] stopDetecting];
    }
    [[EngineUtil shared] deinitEngine];
}

-(void)updateDetail{
    
    NSArray * scoreArray = [_scoreDic allValues];
    int correctScore = 0;
    int wrongScore = 0;
    for(int i = 0; i< scoreArray.count ; i++){
        if ([[scoreArray objectAtIndex:i] intValue] == 1){
            correctScore++;
        } else {
            wrongScore++;
        }
    }
    int unAnswerd = 122 - correctScore - wrongScore;
    
    
    NSMutableDictionary *testObject = [[NSMutableDictionary alloc]
                                       initWithDictionary:@{
                                                            @"tcomplete": @(_isTestComplete),
                                                            @"tnumber" : _testNumber,
                                                            @"cscore" : @(correctScore),
                                                            @"iscore" : @(wrongScore),
                                                            @"nscore" : @(unAnswerd),
                                                            @"objectId": _testNumber,
                                                            @"tnumber" : _testNumber
                                                            }];// removed @"user_id" : [[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"],
    
    
    NSMutableDictionary* phonemDic = [[NSMutableDictionary alloc]init];
    for(NSString* phonemKey in [_scoreDic allKeys]){
        NSString* newKey = [phonemKey stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        if([newKey isEqualToString:@"in"]){
            newKey = [NSString stringWithFormat:@"_%@",newKey];
        }
        if([[_scoreDic valueForKey:phonemKey] integerValue] != 2)
        [phonemDic setValue:[_scoreDic valueForKey:phonemKey] forKey:newKey];
    }
    NSLog(@"%@",phonemDic);
    [testObject addEntriesFromDictionary:phonemDic];
    
    id<IDataStore>dataStore = [backendless.data ofTable:@"ScoringResultDetail"];
    [dataStore save:testObject response:^(id response) {
    } error:^(Fault *fault) {
        
    }];
}

-(void)stopRecordingAndEquilizer{
    
    [[SoundUtil sharedInstance] stopRecorderAndOffset:0];
    [self stopEqualizer];
    _responseLabel.text = PressCompare;
    
}

- (IBAction)listenAction:(id)sender {
    
#warning Remove or comment this alert before send build. Its for testing
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"you have clicked on Listen"
//                                  message:nil
//                                  preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction* yes = [UIAlertAction
//                          actionWithTitle:@"Ok"
//                          style:UIAlertActionStyleCancel
//                          handler:^(UIAlertAction * action){
//
//                          }];
//
//    [alert addAction:yes];
//    [self presentViewController:alert animated:YES completion:nil];

//    [_synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
//    [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    if((indexOfSound - 1) < _phonemeArray.count){
        [self playSoundAtIndex:indexOfSound-1];
    }
    
    
    //old code to play sound// commented on 17-06-2018
//    [_synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
//    [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
//
//
//    NSError* error;
//    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//    [audioSession setCategory:AVAudioSessionCategoryRecord withOptions: AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionAllowBluetoothA2DP | AVAudioSessionCategoryOptionAllowAirPlay error:&error];
//    [audioSession setActive:YES error:nil];
//    if (error) {
//    }
//
//    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord withOptions: AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionAllowBluetoothA2DP | AVAudioSessionCategoryOptionAllowAirPlay error:&error];
//    if (error) {
//    }
//    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:[_soundObject valueForKey:@"sound"] ofType: @"mp3"];
//    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
//    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
//    if(![sender isKindOfClass:[UIButton class]]){
//        _audioPlayer.delegate = self;
//    }
//    _audioPlayer.numberOfLoops = 0; //infinite loop
//    [_audioPlayer prepareToPlay];
//    [_audioPlayer play];
//
//    if (error) {
//    }
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if(!_isTestComplete){
        _soudTimerProgressbar.hidden = TRUE;
        if(IS_TIMER_ENABLE){
            _soudTimerProgressbar.hidden = FALSE;
            _soundObjectTimer = [[MZTimerLabel alloc] initWithLabel:_soundCountLabel andTimerType:MZTimerLabelTypeTimer];
            _soundObjectTimer.timeLabel = _soundCountLabel;
            _soundObjectTimer.timerType = MZTimerLabelTypeTimer;
            [_soundObjectTimer setCountDownTime:SOUND_TIME];
            _soundObjectTimer.timeFormat = @"s";
            _soudTimerProgressbar.value = 0.0;
            [_soundObjectTimer startWithEndingBlock:^(NSTimeInterval countTime) {
                [self nextAction:self];
            }];
            //            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkForTime) userInfo:nil repeats:NO];
        }
    }
}

//-(void)playMessageSound:(NSString*)string{
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//        NSError* error;
//
//        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error: &error];
//
//        AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:string];
//        utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-US"];
//        //    [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
//        [_synthesizer speakUtterance:utterance];
//
//    });
//}

- (void) processSampleData:(NSData *)data{
    
    NSArray* optionsArray = [_soundObject valueForKey:@"options"];
    NSString* wordObject = [_soundObject valueForKey:@"word"];
    
    [self.audioData appendData:data];//Check1
    NSInteger frameCount = [data length] / 2;
    int16_t *samples = (int16_t *) [data bytes];
    int64_t sum = 0;
    for (int i = 0; i < frameCount; i++) {
        sum += abs(samples[i]);
    }
    
    _equlizerImageView.animationDuration = 1.0;
    _equlizerImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld",[self getAnimation:sum/100000]]];
    int chunk_size = 0.1 /* seconds/chunk */ * SAMPLE_RATE * 2 /* bytes/sample */ ; /* bytes/chunk */
    if ([self.audioData length] > chunk_size) {
        if(!_isRecognised){
            _timesRequest ++;
            [[SpeechRecognitionService sharedInstance]
             streamAudioData:self.audioData
             withCompletion:^(StreamingRecognizeResponse *response, NSError *error) {
                 _timesResponse ++;
                 if (error) {
                     _recordingError = error;
                     [self stopAudio:nil];
                     if(error.code ==14){
                         //_responseLabel.text = [error localizedDescription];
                     } else{ //_responseLabel.text = @"Unable to process at this time. h";
                     }
                 } else if (response) {
                     NSLog(@"response = %@",response);
                     if([wordObject isEqualToString:[_soundObject valueForKey:@"word"]]){
                         BOOL finished = NO;
                         for (StreamingRecognitionResult *result in response.resultsArray) {
                             if (result.isFinal) {
                                 finished = YES;
                             }
                             if(_isRecognised)
                             break;
                             for (SpeechRecognitionAlternative *alternative in result.alternativesArray){
                                 if(_isRecognised)
                                 break;
                                 for(NSString * word in optionsArray){
                                     NSString* alternativeString = [[alternative.transcript lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
                                     if([alternativeString isEqualToString: [word lowercaseString]]){
                                         [_scoreDic setValue:@(1) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
                                         [self stopAudio:nil];
                                         //                                     [self nextAction:self];
                                         //_responseLabel.text = [NSString stringWithFormat:@"%@ %@",ComparisonDone,wordObject];
                                         //                                         [self playMessageSound:[NSString stringWithFormat:@"You said it correctly"]];
                                         NSLog(@"You said it correctly3");
                                         [[AudioController sharedInstance] stop];
                                         [[SpeechRecognitionService sharedInstance] stopStreaming];
                                         [self.animatingImageView.layer removeAllAnimations];
                                         
                                         _isRecognised = true;
                                         break;
                                     }
                                 }
                             }
                         }
                         
                         if((_timesResponse == _timesRequest || finished ) && !_isRecognised){
                             //_responseLabel.text = [NSString stringWithFormat:@"Processing done and you did not say it correctly."];
                             //                             [self playMessageSound:[NSString stringWithFormat:@"Sorry! you did not speak it correctly."]];
                         } else {
                             
                         }
                     }
                     
                 } else {
                     if(!_isRecognised){
                         [self.view setUserInteractionEnabled:true];
                         [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(checEmptyResponse) userInfo:nil repeats:NO];
                     }
                     //                     //_responseLabel.text = [NSString stringWithFormat:@"Did not detected any Speech"];
                     //                     [self playMessageSound:[NSString stringWithFormat:@"Sorry! you did not speak it correctly."]];
                 }
             }];
            
        }
        self.audioData = [[NSMutableData alloc] init];
    }
}

-(void)checEmptyResponse{
    [self.view setUserInteractionEnabled:true];
    if(!_isRecognised){
        //_responseLabel.text = [NSString stringWithFormat:@"Try Again!"];
        //        [self playMessageSound:[NSString stringWithFormat:@"Try Again!"]];
    }
}

- (CABasicAnimation *)spinAnimationWithDuration:(CGFloat)duration clockwise:(BOOL)clockwise repeat:(BOOL)repeats{
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    anim.toValue = clockwise ? @(M_PI * 2.0) : @(M_PI * -2.0);
    anim.duration = duration;
    anim.cumulative = YES;
    anim.repeatCount = repeats ? CGFLOAT_MAX : 0;
    return anim;
}

-(void)dealloc{
    _audioPlayer = nil;
    _soudTimerProgressbar = nil;
    _audioData = nil;
    _phonemeArray = nil;
    _soundObject = nil;
    _scoreDic = nil;
    _animatingImageView = nil;
    _soudTimerProgressbar = nil;
    _SoundLabel = nil;
    _soundCountLabel = nil;
    _soundObjectTimer = nil;
    _fullTestTimer = nil;
    _fullTimeCountLabel = nil;
    indexOfSound = 0;
    compared = false;
}

-(NSInteger)getAnimation:(float)frame{
    frame = frame * 10000;
    if(frame >= 4 && frame <= 110 ){
        NSArray * imagesArray = @[@2];
        int index = arc4random() % imagesArray.count;
        return [[imagesArray objectAtIndex:index] integerValue];
    }else  if(frame <= 149 ){
        NSArray * imagesArray = @[@2, @4];
        int index = arc4random() % imagesArray.count;
        return [[imagesArray objectAtIndex:index] integerValue];
    }
    else if( frame >= 150  && frame <= 600){
        NSArray * imagesArray = @[@1, @3, @5, @7];
        int index = arc4random() % imagesArray.count;
        return [[imagesArray objectAtIndex:index] integerValue];
    }
    else if( frame >= 601  && frame <= 1000 ){
        NSArray * imagesArray = @[@9, @8, @6];
        int index = arc4random() % imagesArray.count;
        return [[imagesArray objectAtIndex:index] integerValue];
    }
    else if( frame >= 1000  && frame <= 1500 ){
        NSArray * imagesArray = @[@14, @15, @22 , @27];
        int index = arc4random() % imagesArray.count;
        return [[imagesArray objectAtIndex:index] integerValue];
    }
    else if( frame >= 1500  && frame <= 2600){
        NSArray * imagesArray = @[@11, @20, @27];
        int index = arc4random() % imagesArray.count;
        return [[imagesArray objectAtIndex:index] integerValue];
    }
    else if( frame >= 2600 ){
        NSArray * imagesArray = @[@10, @12, @16 , @18, @19, @21, @23, @24, @26, @28, @17];
        int index = arc4random() % imagesArray.count;
        return [[imagesArray objectAtIndex:index] integerValue];
    }
    
    return 0;
}

-(void)addSoundFile{
    NSURL *soundFileURL = [NSURL fileURLWithPath:[self soundFilePath]];
    NSDictionary *recordSettings = @{AVEncoderAudioQualityKey:@(AVAudioQualityMax),
                                     AVEncoderBitRateKey: @16,
                                     AVNumberOfChannelsKey: @1,
                                     AVSampleRateKey: @(SAMPLE_RATE)};
    NSLog(@"SoundTestViewController Path : %@",soundFileURL.absoluteString);
    NSError *error;
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
    if (error) {
        //            NSLog(@"error: %@", error.localizedDescription);
    }
}

- (NSString *) soundFilePath {
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = dirPaths[0];
    return [docsDir stringByAppendingPathComponent:@"sound.caf"];
}

- (IBAction) processAudio:(id) sender {
    
    
    NSArray* optionsArray = [_soundObject valueForKey:@"options"];
    NSString* wordObject = [_soundObject valueForKey:@"word"];
    
    
    NSString *service = @"https://speech.googleapis.com/v1/speech:recognize";
    service = [service stringByAppendingString:@"?key="];
    service = [service stringByAppendingString:Google_API_KEY];
    
    NSData *audioData = [NSData dataWithContentsOfFile:[self soundFilePath]];
    NSDictionary *configRequest = @{@"encoding":@"LINEAR16",
                                    @"sampleRateHertz":@(SAMPLE_RATE),
                                    @"languageCode":@"en-US",
                                    @"maxAlternatives":@30};
    NSDictionary *audioRequest = @{@"content":[audioData base64EncodedStringWithOptions:0]};
    NSDictionary *requestDictionary = @{@"config":configRequest,
                                        @"audio":audioRequest};
    NSError *error;
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestDictionary
                                                          options:0
                                                            error:&error];
    
    NSString *path = service;
    NSURL *URL = [NSURL URLWithString:path];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    // if your API key has a bundle ID restriction, specify the bundle ID like this:
    [request addValue:[[NSBundle mainBundle] bundleIdentifier] forHTTPHeaderField:@"X-Ios-Bundle-Identifier"];
    NSString *contentType = @"application/json";
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    [request setHTTPMethod:@"POST"];
    NSURLSessionTask *task =
    [[NSURLSession sharedSession]
     dataTaskWithRequest:request
     completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             NSError* error;
             NSDictionary* jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             if(error)
             {
                 if([wordObject isEqualToString:wordObject])
                 {
                     //_responseLabel.text = [NSString stringWithFormat:@"Response received and you did not said it correctly."];
                     //                     [self playMessageSound:[NSString stringWithFormat:@"Sorry! you did not speak it correctly."]];
                 }
                 [_scoreDic setValue:@(0) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
             }
             else if(jsonObject && [jsonObject isKindOfClass:[NSDictionary class]])
             {
                 if(jsonObject.allKeys.count >0)
                 {
                     NSArray *alternativesArray = [[[jsonObject valueForKey:@"results"]valueForKey:@"alternatives"] objectAtIndex:0];
                     NSLog(@"%@",alternativesArray);
                     BOOL isStringRecognised = false;
                     for (NSDictionary *alternative in alternativesArray){
                         if(isStringRecognised)
                         break;
                         for(NSString * word in optionsArray){
                             NSString* alternativeString = [[[alternative valueForKey:@"transcript"] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
                             if([alternativeString isEqualToString: [word lowercaseString]]){
                                 [_scoreDic setValue:@(1) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
                                 if([wordObject isEqualToString:wordObject])
                                 {
                                     //_responseLabel.text = [NSString stringWithFormat:@"Response received and found %@",wordObject];
                                     //                                     [self playMessageSound:[NSString stringWithFormat:@"You said it correctly"]];
                                     NSLog(@"You said it correctly4");
                                 }
                                 isStringRecognised = true;
                                 break;
                             }
                         }
                     }
                     if(!isStringRecognised){
                         if([wordObject isEqualToString:wordObject]){
                             //_responseLabel.text = [NSString stringWithFormat:@"Response received and you did not said it correctly."];
                             //                             [self playMessageSound:[NSString stringWithFormat:@"Sorry! you did not speak it correctly."]];
                         }
                         [_scoreDic setValue:@(0) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
                     }
                 } else {
                     if([wordObject isEqualToString:wordObject]){
                         //_responseLabel.text = [NSString stringWithFormat:@"Try Again!"];
                         //                         [self playMessageSound:[NSString stringWithFormat:@"Try Again!"]];
                     }
                     [_scoreDic setValue:@(0) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
                 }
             }
         });
     }];
    [task resume];
}

- (IBAction)dragoutside:(id)sender {
    //RecordAudio
    if(isRecording){
        [self stopAudio:self];
    }
    NSLog(@"***** IEFT drag outside is called ******");
}

-(void)startTest{
    _fullTestTimer.hidden = TRUE;
    if(IS_TIMER_ENABLE){
        _soudTimerProgressbar.hidden = FALSE;
        _fullTestTimer = [[MZTimerLabel alloc] initWithLabel:_fullTestTimer andTimerType:MZTimerLabelTypeTimer];
        _fullTestTimer.timeLabel = _fullTestTimer;
        _fullTestTimer.timerType = MZTimerLabelTypeTimer;
        [_fullTestTimer setCountDownTime:SOUND_TIME ];
        _fullTestTimer.timeFormat = @"s";
        _soudTimerProgressbar.value = 0.0;
        [_fullTestTimer startWithEndingBlock:^(NSTimeInterval countTime) {
            [self nextAction:self];
        }];
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkForTime) userInfo:nil repeats:NO];
    }
}
//This method is called from bridgeClass to update recording percentage
-(void)recordingProgress:(int)p_nSamples{
    __block float progress = (((float)p_nSamples)/(float)([bridgeClass eg_recordSoundType] * 2))/2;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        if(progress >= 1.0){
            progress = 1.0;
            NSLog(@"Percent: 100");
            // self.lblStatus.text = [NSString stringWithFormat:@"Sound recorded! Press 'Compare' and say the same word."];
        }else{
            NSLog(@"Percent: %f",progress);
            //  self.lblStatus.text = [NSString stringWithFormat:@"Progress: %0.2f",progress*100];
        }
    });
}
//This method is called from bridgeClass to let this class know that recording is stopped
-(void)recordStopped{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        
        [self btnRecordAction:nil];
        [self stopEqualizer];
        
        NSLog(@"\n\n\n********SOUND IS DETECTED \n\n\n************");
        _responseLabel.text = PressCompare;//@"Record for second time"; // this means sound is detected and now we
    });
}
//-(void)onlyStopRecording{
//    [[SoundUtil sharedInstance]onlyStopRecordingAndSavedUnProcessedFile:0];
//    NSString *btnTitle;
//    btnTitle = [[SoundUtil sharedInstance] isRecording] ? Recording : StartRecording;
//    //_responseLabel.text = btnTitle;
//}

-(void)stopEqualizer{
    NSLog(@"stopEqualizer is being called");
    
    [playProgressDisplayLink invalidate];
    playProgressDisplayLink = nil;
    [ self.animatingImageView.layer removeAllAnimations];
    _equlizerImageView.image = nil;
    
    if ([SoundUtil sharedInstance].recorder.isRecording == true){
        [[SoundUtil sharedInstance] stoprRecordingForEqualizer];
    }
    
}

-(void)btnRecordAction:(UIButton*)btn {
    [[SoundUtil sharedInstance]switchRecordingStatus:self AndOffset:0];
    //    NSString *btnTitle;
    //    btnTitle = [[SoundUtil sharedInstance] isRecording] ? Recording : StartRecording;
    //    _responseLabel.text = btnTitle;
}

#pragma mark - Helper methods
-(void)playSoundAtIndex:(int)index{
    [[SoundUtil sharedInstance]playSoundAtIndex:index];
}
#pragma mark - TestSOundAlertDetectionMethods
-(void)stopDetectionForNextSound{
    
    if ([[EngineUtil shared] isDetecting]){
        [[EngineUtil shared]stopDetecting];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:IF_DETECTED_OBSERVER object:nil];
    }
    
}
- (IBAction)btnCompareAction:(id)sender
{
    if([[EngineUtil shared] isDetecting])
    {
        [self stopEqualizer];
        [[EngineUtil shared]stopDetecting];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:IF_DETECTED_OBSERVER object:nil];
        [[SoundUtil sharedInstance] stoprRecordingForEqualizer];
    }
    else
    {
        [self equalizerForCompariosn];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(detectedSound:) name:IF_DETECTED_OBSERVER object:nil];
        [[EngineUtil shared]startDetecting];
    }
}
- (IBAction)btnCompareAction1:(id)sender {
    if([[EngineUtil shared] isDetecting]){
        [self stopEqualizer];
        [[EngineUtil shared]stopDetecting];
        _responseLabel.text = PressCompare;
        [[SoundUtil sharedInstance] stoprRecordingForEqualizer];
        
        
    }else{
        [self equalizerForCompariosn];
        [[EngineUtil shared]startDetecting];
    }
    
}


-(void)stopComparison{
    [[EngineUtil shared]stopDetecting];
    [self stopEqualizer];
    if (compareForTheFirstTime) {
        
    }
    else {
        _responseLabel.text = ComparisonDone;
        
    }
    
    //_responseLabel.text = PressCompare;
    //[[NSNotificationCenter defaultCenter]removeObserver:self name:IF_DETECTED_OBSERVER object:nil];
    if (!compareForTheFirstTime) {  // at start of every sound this value is set at "1" but now its set at 0
        
        //        [self playMessageSound:[NSString stringWithFormat:@"Try Again!"]];
        [_scoreDic setValue:@(0) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
        
    }
    else { // finger took off and sound was detected
        // [self nextAction:nil];
    }
    compareForTheFirstTime = 0;
}
#pragma mark - Sound Detected with Good Accurancy
-(void)detectedSound:(NSNotification*)notify{
    
    NSLog(@"result:::%d",[notify.object boolValue]);
    
    if([notify.object boolValue]){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"call back detected sound");
            [[NSNotificationCenter defaultCenter]removeObserver:self name:IF_DETECTED_OBSERVER object:nil];
            
//            NSString* wordObject = [_soundObject valueForKey:@"word"];
            
            //_responseLabel.text = [NSString stringWithFormat:@"%@ %@",ComparisonDone,wordObject];
            //            [self playMessageSound:[NSString stringWithFormat:@"You said it correctly",wordObject]];//Old code, Corrected by Pavan on 15 April
            //            [self playMessageSound:[NSString stringWithFormat:@"You said it correctly"]];
            NSLog(@"You said it correctly5");
            ////_responseLabel.text =  @"Sound detected correctly"; // this means sound is detected and now we
            
            compared = true;
            [_scoreDic setValue:@(1) forKey:[[_soundObject valueForKey:SOCOREOBJECTKEY] lowercaseString]];
            [self stopEqualizer];
            
            compareForTheFirstTime = 1;
            //Update Status after 4 seconds
            //        double delayInSeconds = 4.0;
            //        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            //        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //            self.lblStatus.text = [NSString stringWithFormat:@"Start recording and say '%@'",[SoundUtil sharedInstance].currentSound[@"word"]];
            //        });
            //    });
            [[EngineUtil shared]stopDetecting];
            
            
        });
    }else{
        [self stopComparison];
    }
}
#pragma mark - UITABLEVIEW DELEGATE
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _phonemeArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IndexCell *cell = (IndexCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"IndexCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    //    cell.lblTitle.text = [[[_phonemeArray objectAtIndex:indexPath.row] valueForKey:@"word"] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    cell.lblTitle.text = [[_phonemeArray objectAtIndex:indexPath.row] valueForKey:@"word"];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    indexOfSound = (int)indexPath.row;
    [self nextClick];
}
@end
