//
//  MenuView.m
//  urok
//
//  Created by Muhammad Ahsan on 6/16/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import "MenuView.h"

@implementation MenuView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"MenuView" owner:self options:nil];
        [self addSubview:self.view];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (void)show{
    self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    [self updateFrames:0];
    self.view.alpha = 0;
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"transparentView"]]];
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:.8 initialSpringVelocity:5 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self updateFrames:ScreenHeight];
        self.view.alpha = 1;
    }completion:^(BOOL finished) {
        
    }];
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:self];
    
    [_voiceSwitch setOn:false];
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"listen_button_enable"]){
        [_voiceSwitch setOn:true];
    }
}

- (IBAction)VoicSwitchAction:(id)sender {
    if(_voiceSwitch.isOn){
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"listen_button_enable"];
        
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"listen_button_enable"];
    }
    self.switchChange();
}

-(void)updateFrames:(float)height{
    CGRect frame = self.view.frame;
    frame.size.width = ScreenWidth;
    frame.size.height = height;
    frame.origin.x = 0;
    frame.origin.y = 0;
    self.view.frame = frame;
}

-(void)hideWithCompletionHandler:(void (^)())completionHandler{
    self.alpha = 1.0;
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:.8 initialSpringVelocity:5 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.alpha = 0.0;
        self.frame = CGRectMake(0, 0, ScreenWidth, 0);
        self.closeButton.transform = CGAffineTransformMakeRotation(-3.14/4);
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
        completionHandler();
    }];
}
- (IBAction)closeAction:(id)sender {
    [self hideWithCompletionHandler:^() {
        
    }];
}


- (IBAction)instructionsAction:(id)sender {
    [appdelegate setInstructionsViewController];
    [self hideWithCompletionHandler:^() {
    }];
}

@end
