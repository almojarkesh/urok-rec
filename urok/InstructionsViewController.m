//
//  InstructionsViewController.m
//  urok
//
//  Created by Muhammad Ahsan on 6/16/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import "InstructionsViewController.h"
#import "SoundManager.h"

@interface InstructionsViewController ()

@end

@implementation InstructionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)getStartedAction:(id)sender {
    
    [[SoundManager sharedInstance] stopPlayer];
    
    [appdelegate setSoundTestViewController];
    
}

- (IBAction)instructionsAction:(id)sender {
    [[SoundManager sharedInstance] playSound:@"App instructions" type:@"m4a"];
}

@end
