//
//  EngineUtil.m
//  urok
//
//  Created by Chanchal Raj on 04/08/2017.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import "EngineUtil.h"
#import "SoundUtil.h"
static EngineUtil *appSharedData_ = nil;
@implementation EngineUtil
bool isLoadingFiles; // A variable that keeps track of sound files loading process while initializing engine
+(instancetype)shared
{
    static dispatch_once_t predicate;
    if(appSharedData_ == nil){
        dispatch_once(&predicate,^{
            appSharedData_ = [[EngineUtil alloc] init];
            
        });
    }
    return appSharedData_;
}
-(void)initEngine{
    [bridgeClass set_g_pDetectMgr_SAMPLE_FREQ];
    [bridgeClass set_g_isEngineTerminated:false];
    [bridgeClass set_g_bDetecting:false];
    [bridgeClass initEngine];
    [self loadThresholdDeltas];
    [self initData];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self startDetectProcess];
    });
}
-(void) deinitEngine{
    [bridgeClass terminateEngine];
}
-(void)startDetecting{
    [bridgeClass set_g_bDetecting:true];
}
-(BOOL)isDetecting{
    return [bridgeClass get_g_bDetecting];
}
-(void)switchEngineState{
    if ([bridgeClass get_g_bDetecting])
        [self stopDetecting];
    else
        [self startDetecting];
}

-(void)stopDetecting{
    [bridgeClass set_g_bDetecting:false];
    [bridgeClass clearFFTValues];
}
-(void)startRecording{
    [bridgeClass RecordButtonStart];
}
-(void)continueRecording{
    [bridgeClass RecordButtonPressing];
}
-(void)stopRecording{
    [bridgeClass stopRecordButton];
}

//Helper Methods
-(void)loadThresholdDeltas{
    NSString * strKey = @"";
    NSString * strMode = @"Home";
    int RST_COUNT = [bridgeClass get_RST_COUNT];
    for (int i = 0; i< RST_COUNT; i++){
        strKey = [NSString stringWithFormat:@"MATCH_RATE_DELTA_%@_%d",strMode,i];
        [bridgeClass set_MATCHING_RATE_THRESHOLD_DELTAS:i :strKey];
    }
    strKey = [NSString stringWithFormat:@"UNIVERSAL_THRESHOLD_DELTA_%@",strMode];
    [bridgeClass set_UNIVERSAL_THRESHOLD_DELTA:strKey];
}
-(void)initData{
    isLoadingFiles = true;
    int RST_COUNT = [bridgeClass get_RST_COUNT];

    NSLog(@"\n rst count is :%d\n" ,RST_COUNT);
    for(int i = 0;i<RST_COUNT;i++){
       
        for  (int j=0; j< 1; j++) {
            NSString *strDetectPath = [bridgeClass getDetectPath1:@"Home Mode" recordSoundType:i andOffset:j];
            NSString *strDet = [strDetectPath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
            NSLog(@"\n path of sound %@ and sound index %d",strDet,i);
            [bridgeClass initData:strDet :i];
        
        }
    }
    isLoadingFiles = false;
}
-(void)startDetectProcess{
    //[bridgeClass set_g_bDetecting:true];
    int nAlarmType = 0;
    int nAlarmIndex = 0;
    NSLog(@"entered start detect");
    BOOL alredayDetected = false;
    while(TRUE){
        //NSLog(@"while 1");
        if([bridgeClass get_g_isEngineTerminated]){
            //NSLog(@"while 2");
            break;
        }
        while([bridgeClass get_ReadData:[bridgeClass get_g_fBufData] :[bridgeClass get_FRAME_LEN]]){
            if([bridgeClass get_g_isEngineTerminated]){
                //NSLog(@"while 3");
                break;
            }
            //NSLog(@"while 4");
            if ([bridgeClass get_g_pDetectMgr_isnill]){
                //NSLog(@"is nill true");
                if([bridgeClass stopPressingWithDetected]){
                    NSLog(@"sound detection complete:%d",alredayDetected);
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DetectedSound" object:@(alredayDetected)];
                }
                
                if ([bridgeClass get_g_bDetecting] || [bridgeClass get_g_pDetectMgr_IsRecordingOrPreparing]){
                    
                    if (!isLoadingFiles){
                       if ([bridgeClass get_g_pDetectMgr_Process:[bridgeClass get_g_fBufData] :[bridgeClass get_FRAME_LEN] :[[SoundUtil sharedInstance]indexOfCurrentSound] :nAlarmIndex :[[SoundUtil sharedInstance]indexOfCurrentSound]]){
                            NSLog(@"detected!!! and index %d",[[SoundUtil sharedInstance]indexOfCurrentSound]);
                           //[[NSNotificationCenter defaultCenter]postNotificationName:@"DetectedSound" object:nil];
                           alredayDetected = true;
                            //Detected nAlertType tell ur UI
                            //currentDetectionSoundName = "\(arrSoundType.object(at: self.nAlarmType))"
                            //currentDetectionSoundDesc = "\(arrDescription.object(at: self.nAlarmType))"
                        }
                        if ([bridgeClass get_g_pDetectMgr_Process:[bridgeClass get_g_fBufData] :[bridgeClass get_FRAME_LEN] :[[SoundUtil sharedInstance]indexOfCurrentSound] :nAlarmIndex :[[SoundUtil sharedInstance]indexOfCurrentSound]]){
                            NSLog(@"detected and compared with second sound!!! and index %d",[[SoundUtil sharedInstance]indexOfCurrentSound] + [[SoundUtil sharedInstance] getOffset]);
                            //[[NSNotificationCenter defaultCenter]postNotificationName:@"DetectedSound" object:@(1)];
                            alredayDetected = true;
                            //Detected nAlertType tell ur UI
                            //currentDetectionSoundName = "\(arrSoundType.object(at: self.nAlarmType))"
                            //currentDetectionSoundDesc = "\(arrDescription.object(at: self.nAlarmType))"
                        }
                    }
                }else{//end recording
//                    if([bridgeClass stopPressingWithDetected]){
//                        NSLog(@"recording end.....detected:%d",alredayDetected);
//                        [[NSNotificationCenter defaultCenter]postNotificationName:@"DetectedSound" object:@(alredayDetected)];
//                    }
                    //
                }//end
            }
        }
        usleep(150000);
    }
}
-(void)updateMatchingThreshhold:(float)threshhold{
    [bridgeClass updateMatchingThreshhold:threshhold];
}

-(void)stopRecordingWithDetectedSound{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"DetectedSound:" object:@(0)];
}
@end
