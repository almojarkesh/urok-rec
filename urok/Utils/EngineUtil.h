//
//  EngineUtil.h
//  urok
//
//  Created by Chanchal Raj on 04/08/2017.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "bridgeClass.h"
@interface EngineUtil : NSObject
+(instancetype)shared;
-(void)initEngine;
-(void)startDetecting;
-(void)stopDetecting;
-(void)switchEngineState;
-(BOOL)isDetecting;
-(void) deinitEngine;
-(void)startRecording;
-(void)continueRecording;
-(void)stopRecording;
-(void)stopRecordingWithDetectedSound;
-(void)updateMatchingThreshhold:(float)threshhold;
@end
