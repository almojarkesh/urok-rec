//
//  AppDelegate.m
//  urok
//
//  Created by Muhammad Ahsan on 6/15/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import "AppDelegate.h"
#import "InstructionsViewController.h"
#import "SoundTestViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <AVFoundation/AVFoundation.h>
#import "Config.h"

//let APP_ID = "YOUR-APPLICATION-ID"
//let API_KEY = "YOUR-APPLICATION-IOS-API-KEY"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[Backendless sharedInstance] initApp:APP_ID APIKey:API_KEY];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"listen_button_enable"];
    [Fabric with:@[[Crashlytics class]]];
    [self setSoundTestViewController];
    
    self.showAlertForFrequncy = false;
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
 //   [[Crashlytics sharedInstance] crash];

    return YES;
}

-(void)setInstructionsViewController{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    InstructionsViewController * controller = [storyboard instantiateViewControllerWithIdentifier:@"InstructionsViewController"];
    [self.window setRootViewController:controller];
    [self.window makeKeyAndVisible];
}


-(void)setSoundTestViewController{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SoundTestViewController * controller = [storyboard instantiateViewControllerWithIdentifier:@"SoundTestViewController"];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:controller];
    [navController setNavigationBarHidden:YES];
    [self.window setRootViewController:navController];
    [self.window makeKeyAndVisible];
}


//-(void)playMessageSound:(NSString*)string{
//
//    NSLog(@"Ahsan");
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//        NSError* error;
//        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error: &error];
//
//        AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:string];
//        utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-US"];
//        //    [_synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
//        //        [_synthesizer speakUtterance:utterance];
//        if(error)
//            NSLog(@"%@",error);
//    });
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
