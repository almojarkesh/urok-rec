//
//  SoundTestViewController.h
//  urok
//
//  Created by Muhammad Ahsan on 6/16/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndexCell.h"

@interface SoundTestViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
-(void)recordStopped;
-(void)recordingProgress:(int)p_nSamples;
@property (weak, nonatomic) IBOutlet UITableView *tblData;
//-(void)onlyStopRecording;
@end
