//
//  AppDelegate.h
//  urok
//
//  Created by Muhammad Ahsan on 6/15/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

#define UROKSOUND  @"urok"
#define SOUNDALERT  @"shortalgorithm"
#define SIRI  @"Siri"

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSMutableDictionary* user_dict;
@property (nonatomic, assign) BOOL showAlertForFrequncy;
@property (nonatomic, assign) BOOL isMaxFrequncy;
@property (nonatomic) float maxFrequncyOfVoice;

-(void)setInstructionsViewController;
-(void)setSoundTestViewController;
@end

