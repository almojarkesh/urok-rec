//
//  SoundUtil.h
//  urok
//
//  Created by Muhammad Ahsan on 6/17/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface SoundUtil : NSObject

@property (nonatomic,retain) AVAudioRecorder *recorder;

@property (nonatomic,retain) AVAudioPlayer *audioPlayer;

+(SoundUtil *) sharedInstance;
-(NSMutableArray*)getSounds:(BOOL)isDeviceTest;
@property (nonatomic,retain) NSDictionary *currentSound;
-(int)indexOfCurrentSound;
@property (nonatomic,retain) NSMutableArray *soundsArray;
@property (nonatomic,retain) NSMutableArray *soundsToUpload;
//changeMade
-(void)initializeRecorderForSoundId:(uint)soundType andOffset:(int)offset;
-(void)switchRecordingStatus:(UIViewController *)VC AndOffset:(int)offset;
-(void)setCurrentSound:(NSDictionary*)sound;
-(void)playSoundAtIndex:(uint)index;
-(BOOL)isRecording;
-(void)stopPlaying;

-(int)getOffset;
-(void)stopRecorderAndOffset:(int)offsetIndex;
-(void)startRecordingForEqualizer;
-(void)stoprRecordingForEqualizer;
-(void)onlyStopRecordingAndSavedUnProcessedFile:(int)offsetIndex;
@end
