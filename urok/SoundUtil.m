//
//  SoundUtil.m
//  urok
//
//  Created by Muhammad Ahsan on 6/17/17.
//  Copyright © 2017 Accuretech. All rights reserved.
//

#import "SoundUtil.h"
#import "bridgeClass.h"

@implementation SoundUtil
@synthesize soundsArray,soundsToUpload;
@synthesize recorder;
//AVAudioRecorder *recorder;
AVAudioPlayer *audioPlayer;

int offset;
+(SoundUtil *) sharedInstance {
    static SoundUtil *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[SoundUtil alloc] init];
        offset = 0;
    });
    return _sharedInstance;
}
BOOL isRecording;

-(id)init{
    if (self=[super init]) {
    }
    return self;
}

#pragma mark - Get Sound
-(NSMutableArray*)getSounds:(BOOL)isDeviceTest{
    soundsArray = [[NSMutableArray alloc]init];
    
    
    if (!isDeviceTest){
        
        
        [soundsArray addObject:@{@"id" : @(1),
                                 @"word" : @"p",
                                 @"wordMatch" :@[@"P"],
                                 @"sound" : @"P",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"p",
                                 @"options" : @[@"P"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(2),
                                 @"word" : @"b",
                                 @"wordMatch" :@[@"b"],
                                 @"sound" : @"B",
                                 @"type" : @"Siri",
                                 @"databaseValue" : @"b",
                                 @"options" : @[@"But", @"Babe", @"best", @"be", @"better" , @"bill", @"good", @"be good", @"bar"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(3),
                                 @"word" : @"t",
                                 @"wordMatch" :@[@"t"],
                                 @"sound" : @"T and Ed",
                                 @"type" : @"Siri",
                                 @"databaseValue" : @"t",
                                 @"options" : @[@"It", @"at", @"eight"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(4),
                                 @"word" : @"d",
                                 @"wordMatch" :@[@"d"],
                                 @"sound" : @"D and Ed",
                                 @"type" : @"Siri",
                                 @"databaseValue" : @"d",
                                 @"options" : @[@"Did", @"bad", @"day", @"done",@"do", @"dick", @"day", @"the" ]
                                 
                                 }];
        
        [soundsArray addObject:@{@"id" : @(5),
                                 @"word" : @"k",
                                 @"wordMatch" :@[@"k"],
                                 @"sound" : @"K and C and Ch and Ck",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"k",
                                 @"options" : @[@"Pick", @"take", @"check", @"nick"]
                                 
                                 }];
        
        [soundsArray addObject:@{@"id" : @(6),
                                 @"word" : @"g \n (hard sound)",
                                 @"wordMatch" :@[@"g"],
                                 @"sound" : @"G and Gh",
                                 @"type" : @"Siri",
                                 @"databaseValue" : @"g_hard_sound",
                                 @"options" : @[@"Gig", @"get",@"good"]
                                 
                                 }];
        
        [soundsArray addObject:@{@"id" : @(7),
                                 @"word" : @"g \n (soft sound)",
                                 @"wordMatch" :@[@"g"],
                                 @"sound" : @"J and soft G and Dge",
                                 @"type" : @"Siri",
                                 @"databaseValue" : @"g_soft_sound",
                                 @"options" : @[@"J", @"just",@"do you", @"jim", @"jack", @"George", @"G"]
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(8),
//                                 @"word" : @"c \n (hard sound)",
//                                 @"wordMatch" :@[@"c"],
//                                 @"sound" : @"K and C and Ch and Ck",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"c_hard_sound",
//                                 @"options" : @[@"Pick", @"take", @"check", @"nick"]
//                                 }];
        
        [soundsArray addObject:@{@"id" : @(9),
                                 @"word" : @"c \n (soft sound)",
                                 @"wordMatch" :@[@"c"],
                                 @"sound" : @"S soft",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"c_soft_sound",
                                 @"options" : @[@"Six", @"yes", @"sis", @"6", @"66", @"S", @"sex", @"for",@"four"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(10),
                                 @"word" : @"f",
                                 @"wordMatch" :@[@"f"],
                                 @"sound" : @"F and Ph and Gh",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"f",
                                 @"options" : @[@"If"]
                                 
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(11),
//                                 @"word" : @"ph",
//                                 @"wordMatch" :@[@"ph"],
//                                 @"sound" : @"F and Ph and Gh",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ph",
//                                 @"options" : @[@"If"]
//                                 }];
        
//        [soundsArray addObject:@{@"id" : @(12),
//                                 @"word" : @"gh \n (ex: tough)",
//                                 @"wordMatch" : @[@"gh"],
//                                 @"sound" : @"F and Ph and Gh",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"gh_ex_tough",
//                                 @"options" : @[@"If"]
//
//                                 }];
//        [soundsArray addObject:@{@"id" : @(13),
//                                 @"word" : @"gh \n (ex: ghost)",
//                                 @"wordMatch" :@[@"gh"],
//                                 @"sound" : @"G and Gh",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"gh_ex_ghost",
//                                 @"options" : @[@"Gig", @"get",@"good"]
//                                 }];
        
        [soundsArray addObject:@{@"id" : @(14),
                                 @"word" : @"v",
                                 @"wordMatch" :@[@"v"],
                                 @"sound" : @"V",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"v",
                                 @"options" : @[@"If", @"it is", @"it", @"is"]
                                 
                                 }];
        [soundsArray addObject:@{@"id" : @(15),
                                 @"word" : @"th \n (noisy sound)",
                                 @"wordMatch" :@[@"th",@"s",@"f"],
                                 @"sound" : @"Th noise",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"th_noisy_sound",
                                 @"options" : @[@"If", @"it is", @"it", @"is"]
                                 
                                 }];
        [soundsArray addObject:@{@"id" : @(16),
                                 @"word" : @"th \n (quiet sound)",
                                 @"wordMatch" :@[@"th",@"s",@"f"],
                                 @"sound" : @"Th soft",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"th_quiet_sound",
                                 @"options" : @[@"If", @"it is", @"it", @"is"]
                                 
                                 }];
//        [soundsArray addObject:@{@"id" : @(17),
//                                 @"word" : @"s \n (soft sound)",
//                                 @"wordMatch" :@[@"s"],
//                                 @"sound" : @"S soft",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"s_soft_sound",
//                                 @"options" : @[@"Six", @"yes", @"sis", @"6", @"66", @"S", @"sex", @"for",@"four"]
//
//                                 }];
    
        [soundsArray addObject:@{@"id" : @(18),
                                 @"word" : @"s \n (hard sound)",
                                 @"wordMatch" :@[@"s"],
                                 @"sound" : @"Z and S and X",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"s_hard_sound",
                                 @"options" : @[@"Zzz", @"z", @"zoo",@"is this", @"zz", @"z", @"zeus", @"xx", @"x"]
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(19),
//                                 @"word" : @"z",
//                                 @"wordMatch" :@[@"z"],
//                                 @"sound" : @"Z and S and X",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"z",
//                                 @"options" : @[@"Zzz", @"z", @"zoo",@"is this", @"zz", @"z", @"zeus", @"xx", @"x"]
//                                 }];
    
        [soundsArray addObject:@{@"id" : @(20),
                                 @"word" : @"sh",
                                 @"wordMatch" :@[@"sh"],
                                 @"sound" : @"Sh and Ch",
                                 @"type" : @"Siri",
                                 @"databaseValue" : @"sh",
                                 @"options" : @[@"Shush"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(21),
                                 @"word" : @"ch \n (ex: chip)",
                                 @"wordMatch" :@[@"ch"],
                                 @"sound" : @"Ch and Tch",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"ch_ex_chip",
                                 @"options" : @[@"Each", @"Which", @"Church", @"H",@"catch", @"touch"]
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(22),
//                                 @"word" : @"ch \n (ex: chrome)",
//                                 @"wordMatch" :@[@"ch"],
//                                 @"sound" : @"K and C and Ch and Ck",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ch_ex_chrome",
//                                 @"options" : @[@"Pick", @"take", @"check", @"nick"]
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(23),
//                                 @"word" : @"ch \n (ex: chef)",
//                                 @"wordMatch" :@[@"sh"],
//                                 @"sound" : @"Sh and Ch",
//                                 @"type" : @"Siri",
//                                 @"databaseValue" : @"ch_ex_chef",
//                                 @"options" : @[@"Shush"]
//
//                                 }];
//        [soundsArray addObject:@{@"id" : @(24),
//                                 @"word" : @"j",
//                                 @"wordMatch" :@[@"j"],
//                                 @"sound" : @"J and soft G and Dge",
//                                 @"type" : @"Siri",
//                                 @"databaseValue" : @"j",
//                                 @"options" : @[@"J", @"just",  @"do you", @"jim", @"jack", @"George", @"G"]
//                                 
//                                 }];
        
        [soundsArray addObject:@{@"id" : @(25),
                                 @"word" : @"l",
                                 @"wordMatch" :@[@"l"],
                                 @"sound" : @"L and Tle",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"l",
                                 @"options" : @[@"All", @"Oh", @"o", @"0",@"LOL"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(26),
                                 @"word" : @"r",
                                 @"wordMatch" :@[@"r"],
                                 @"sound" : @"R and Wr",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"r",
                                 @"options" : @[@"Earn", @"are", @"error", @"hello",@"for"]
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(27),
//                                 @"word" : @"wr",
//                                 @"wordMatch" :@[@"wr",@"w"],
//                                 @"sound" : @"R and Wr",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"wr",
//                                 @"options" : @[@"Earn", @"are", @"error", @"hello",@"for"]
//                                 }];
    
        [soundsArray addObject:@{@"id" : @(28),
                                 @"word" : @"w",
                                 @"wordMatch" :@[@"w"],
                                 @"sound" : @"W",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"w",
                                 @"options" : @[@"W"]
                                 }];
        [soundsArray addObject:@{@"id" : @(29),
                                 @"word" : @"h",
                                 @"wordMatch" :@[@"h"],
                                 @"sound" : @"H",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"h",
                                 @"options" : @[@"H"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(30),
                                 @"word" : @"wh",
                                 @"wordMatch" :@[@"wh",@"w"],
                                 @"sound" : @"Wh",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"wh",
                                 @"options" : @[@"Wh"]
                                 }];
        
        
        [soundsArray addObject:@{@"id" : @(31),
                                 @"word" : @"m",
                                 @"wordMatch" :@[@"m"],
                                 @"sound" : @"M",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"m",
                                 @"options" : @[@"Moon", @"mum", @"I’m. In", @"And" ]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(32),
                                 @"word" : @"n",
                                 @"wordMatch" :@[@"n"],
                                 @"sound" : @"N and Kn",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"n",
                                 @"options" : @[@"Moon", @"nine", @"in", @"non", @"an",@"Knowing", @"And"]
                                 
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(33),
//                                 @"word" : @"kn",
//                                 @"wordMatch" :@[@"k",@"kn"],
//                                 @"sound" : @"N and Kn",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"kn",
//                                 @"options" : @[@"Moon", @"nine", @"in", @"non", @"an",@"Knowing", @"And"]
//                                 }];
        [soundsArray addObject:@{@"id" : @(34),
                                 @"word" : @"ng",
                                 @"wordMatch" :@[@"n",@"ng"],
                                 @"sound" : @"Ng",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"ng",
                                 @"options" : @[@"Long", @"wrong", @"and", @"when", @"one", @"morning", @"long",@"Thank", @"In"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(35),
                                 @"word" : @"nk",
                                 @"wordMatch" :@[@"n",@"nk"],
                                 @"sound" : @"Nk",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"nk",
                                 @"options" : @[@"Inc", @"Mink", @"Make", @"Mike",@"Mick", @"Nick", @"Milk", @"In", @"Thank you"]
                                 }];
        
        
        [soundsArray addObject:@{@"id" : @(36),
                                 @"word" : @"qu",
                                 @"wordMatch" :@[@"qu",@"qa",@"q"],
                                 @"sound" : @"Qu",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"qu",
                                 @"options" : @[@"Call", @"Quick", @"quit"]
                                 }];
        
        
        
        [soundsArray addObject:@{@"id" : @(37),
                                 @"word" : @"y (consonant) \n (ex :yes)",
                                 @"sound" : @"Yu",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"y_consonant_ex_yes",
                                 @"options": @[
                                         @"ya"
                                         ]}];
        
        
        [soundsArray addObject:@{@"id" : @(38),
                                 @"word" : @"x \n (ex : box)",
                                 @"wordMatch" :@[@"x"],
                                 @"sound" : @"Ks",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"x_ex_box",
                                 @"options" : @[@"X"]
                                 
                                 }];
//        [soundsArray addObject:@{@"id" : @(39),
//                                 @"word" : @"x \n (ex : xylophone)",
//                                 @"wordMatch" :@[@"x"],
//                                 @"sound" : @"Z and S and X",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"x_ex_xylophone",
//                                 @"options" : @[@"Zzz", @"z", @"zoo",@"is this", @"zz", @"z", @"zeus", @"xx", @"x"]
//
//                                 }];
    
        [soundsArray addObject:@{@"id" : @(40),
                                 @"word" : @"x \n (ex : exam)",
                                 @"wordMatch" :@[@"g",@"gz",@"z"],
                                 @"sound" : @"Gz",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"x_ex_exam",
                                 @"options" : @[@"Guess", @"guss"]
                                 
                                 }];
        
        
//        [soundsArray addObject:@{@"id" : @(41),
//                                 @"word" : @"ck",
//                                 @"wordMatch" :@[@"ck"],
//                                 @"sound" : @"K and C and Ch and Ck",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ck",
//                                 @"options" : @[@"Pick", @"take", @"check", @"nick"]
//
//                                 }];
    
//        [soundsArray addObject:@{@"id" : @(42),
//                                 @"word" : @"tch",
//                                 @"wordMatch" :@[@"ch",@"tch"],
//                                 @"sound" : @"Ch and Tch",
//                                 @"type" : @"Siri",
//                                 @"databaseValue" : @"tch",
//                                 @"options" : @[@"Each", @"Which", @"Church", @"H",@"catch", @"touch"]
//
//                                 }];
//
//
//        [soundsArray addObject:@{@"id" : @(43),
//                                 @"word" : @"dge",
//                                 @"wordMatch" :@[@"dge",@"ge"],
//                                 @"sound" : @"J and soft G and Dge",
//                                 @"type" : @"Siri",
//                                 @"databaseValue" : @"dge",
//                                 @"options" : @[@"J", @"just",@"do you", @"jim", @"jack", @"George", @"G"]
//
//
//
//                                 }];
    
        
        //       **************Vowels******************
        
        [soundsArray addObject:@{@"id" : @(44),
                                 @"word" : @"i \n (short)",
                                 @"wordMatch" :@[@"i"],
                                 @"sound" : @"I short and Y",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"i_short",
                                 @"options" : @[@"I"]
                                 
                                 }];
        
        [soundsArray addObject:@{@"id" : @(45),
                                 @"word" : @"i \n (long)",
                                 @"sound" : @"I hard and Y",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"i_long",
                                 @"options": @[
                                         @"I",
                                         ]}];
        
        
        [soundsArray addObject:@{@"id" : @(46),
                                 @"word" : @"e \n (short)",
                                 @"wordMatch" :@[@"e"],
                                 @"sound" : @"E short",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"e_short",
                                 @"options" : @[@"E"]
                                 
                                 }];
        [soundsArray addObject:@{@"id" : @(47),
                                 @"word" : @"e \n (long)",
                                 @"sound" : @"E hard and Y",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"e_long",
                                 @"options": @[@"e"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(48),
                                 @"word" : @"a \n (short)",
                                 @"wordMatch" :@[@"a"],
                                 @"sound" : @"A short",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"a_short",
                                 @"options" : @[@"After", @"a", @"App", @"I’m", @"Oh"]
                                 
                                 }];
        
        
        [soundsArray addObject:@{@"id" : @(49),
                                 @"word" : @"a \n (long)",
                                 @"sound" : @"A hard",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"a_long",
                                 @"options": @[
                                         @"A",
                                         ]}];
        
        [soundsArray addObject:@{@"id" : @(50),
                                 @"word" : @"u \n (short)",
                                 @"wordMatch" :@[@"u"],
                                 @"sound" : @"U short",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"u_short",
                                 @"options" : @[@"U"]
                                 
                                 }];
        
        
        [soundsArray addObject:@{@"id" : @(51),
                                 @"word" : @"u \n (long)",
                                 @"sound" : @"U",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"u_long",
                                 @"options": @[
                                         @"u",
                                         @"you"
                                         ]}];
        
        [soundsArray addObject:@{@"id" : @(52),
                                 @"word" : @"o \n (short)",
                                 @"wordMatch" :@[@"o"],
                                 @"sound" : @"O short",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"o_short",
                                 @"options": @[@"Oh",@"Are", @"O",@"All", @"In"]
                                 }];
        
        [soundsArray addObject:@{@"id" : @(53),
                                 @"word" : @"o \n (long)",
                                 @"sound" : @"O hard and Ow",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"o_long",
                                 @"options": @[
                                         @"o"
                                         ]}];
        
        
//        [soundsArray addObject:@{@"id" : @(54),
//                                 @"word" : @"y \n (ex : gym)",
//                                 @"wordMatch" :@[@"y"],
//                                 @"sound" : @"I short and Y",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"y_ex_gym",
//                                 @"options":@[@"I"]
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(55),
//                                 @"word" : @"y \n (ex : fly)",
//                                 @"sound" : @"I hard and Y",
//                                 @"type" : @"urok",
//                                 @"databaseValue" : @"y_ex_fly",
//                                 @"options": @[
//                                         @"I"
//                                         ]}];
//
//        [soundsArray addObject:@{@"id" : @(56),
//                                 @"word" : @"y \n (ex : windy)",
//                                 @"sound" : @"E hard and Y",
//                                 @"type" : @"urok",
//                                 @"databaseValue" : @"y_ex_windy",
//                                 @"options": @[
//                                         @"e"
//                                         ]}];
    
        [soundsArray addObject:@{@"id" : @(57),
                                 @"word" : @"ble",
                                 @"sound" : @"Ble",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"ble",
                                 @"options": @[
                                         @"Ble",
                                         @"Ble",
                                         @"Ball",
                                         @"Bald",
                                         @"Ba",
                                         @"Bow",
                                         ]}];
        
        [soundsArray addObject:@{@"id" : @(58),
                                 @"word" : @"dle",
                                 @"options": @[
                                         @"dall",
                                         @"doll",
                                         ],
                                 @"sound" : @"Dle",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"dle"
                                 }];
        
        [soundsArray addObject:@{@"id" : @(59),
                                 @"word" : @"ple",
                                 @"options": @[
                                         @"Ple",
                                         @"pull",
                                         @"Paul",
                                         @"Po",
                                         @"Pu",
                                         @"pool"
                                         ],
                                 @"sound" : @"Ple",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"ple"
                                 }];
        
        [soundsArray addObject:@{@"id" : @(60),
                                 @"word" : @"tle",
                                 @"options": @[
                                         @"tall",
                                         @"tell",
                                         ],
                                 @"sound" : @"Tle",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"tle"
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(61),
//                                 @"word" : @"tle \n (silent t)",
//                                 @"wordMatch" :@[@"tle",@"le"],
//                                 @"sound" : @"L and Tle",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"tle_silent_t",
//                                 @"options": @[@"All", @"Oh", @"O", @"0"]
//
//                                 }];
    
        [soundsArray addObject:@{@"id" : @(62),
                                 @"word" : @"gle",
                                 @"options": @[
                                         @"Gle",
                                         @"go",
                                         ],
                                 @"sound" : @"Gle",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"gle"
                                 }];
        
        [soundsArray addObject:@{@"id" : @(63),
                                 @"word" : @"fle",
                                 @"sound" : @"Fle",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"fle",
                                 @"options": @[
                                         @"Ful",
                                         @"for",
                                         @"full"
                                         ]}];
        
        
        [soundsArray addObject:@{@"id" : @(64),
                                 @"word" : @"kle",
                                 @"sound" : @"Kle and Cle",
                                 @"databaseValue" : @"kle",
                                 @"options": @[
                                         @"call",
                                         @"cole",
                                         @"coal",
                                         @"ko",
                                         @"cool",
                                         
                                         ],
                                 @"type" : @"urok"
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(65),
//                                 @"word" : @"cle",
//                                 @"sound" : @"Kle and Cle",
//                                 @"databaseValue" : @"cle",
//                                 @"options": @[
//                                         @"call",
//                                         @"cole",
//                                         @"coal",
//                                         @"ko",
//                                         @"cool",
//
//                                         ],
//                                 @"type" : @"urok"
//                                 }];
    
        
        [soundsArray addObject:@{@"id" : @(66),
                                 @"word" : @"zle",
                                 @"databaseValue" : @"zle",
                                 @"options": @[
                                         @"zull",
                                         @"zul",
                                         @"zoll",
                                         @"zol",
                                         @"zoo"
                                         ],
                                 @"sound" : @"Zle",
                                 @"type" : @"urok"}];
        
        //     **Control R's***
        
        [soundsArray addObject:@{@"id" : @(67),
                                 @"word" : @"or \n (ex : for)",
                                 @"sound" : @"Or and Ar",
                                 @"options": @[
                                         @"Or",
                                         @"all",
                                         ],
                                 @"type" : @"urok",
                                 @"databaseValue" : @"or_ex_for"
                                 }];
        
        
        
        [soundsArray addObject:@{@"id" : @(68),
                                 @"word" : @"or \n (ex : doctor)",
                                 @"sound" : @"Er and Ur and Ir and Or and Ar",
                                 @"options": @[
                                         @"Er",
                                         @"ir",
                                         @"air",
                                         @"her",
                                         @"eal",
                                         @"erreur",
                                         @"errors",
                                         @"everfi",
                                         @"errar",
                                         @"every",
                                         @"ever ever",
                                         @"error error",
                                         @"Terror",
                                         @"airer",
                                         @"ever",
                                         @"heir",
                                         @"err",
                                         @"airer",
                                         @"airfare",
                                         @"Ayr",
                                         @"Ayer",
                                         
                                         ],
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"or_ex_doctor"
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(69),
//                                 @"word" : @"ur",
//                                 @"sound" : @"Er and Ur and Ir and Or and Ar",
//
//                                 @"options": @[
//                                         @"Er",
//                                         @"ir",
//                                         @"air",
//                                         @"her",
//                                         @"eal",
//                                         @"erreur",
//                                         @"errors",
//                                         @"everfi",
//                                         @"errar",
//                                         @"every",
//                                         @"ever ever",
//                                         @"error error",
//                                         @"Terror",
//                                         @"airer",
//                                         @"ever",
//                                         @"heir",
//                                         @"err",
//                                         @"airer",
//                                         @"airfare",
//                                         @"Ayr",
//                                         @"Ayer",
//
//                                         ],
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ur"
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(70),
//                                 @"word" : @"ir",
//                                 @"sound" : @"Er and Ur and Ir and Or and Ar",
//                                 @"options": @[
//                                         @"Er",
//                                         @"ir",
//                                         @"air",
//                                         @"her",
//                                         @"eal",
//                                         @"erreur",
//                                         @"errors",
//                                         @"everfi",
//                                         @"errar",
//                                         @"every",
//                                         @"ever ever",
//                                         @"error error",
//                                         @"Terror",
//                                         @"airer",
//                                         @"ever",
//                                         @"heir",
//                                         @"err",
//                                         @"airer",
//                                         @"airfare",
//                                         @"Ayr",
//                                         @"Ayer",
//
//                                         ],
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ir"
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(71),
//                                 @"word" : @"er",
//                                 @"sound" : @"Er and Ur and Ir and Or and Ar",
//                                 @"options": @[
//                                         @"Er",
//                                         @"ir",
//                                         @"air",
//                                         @"her",
//                                         @"eal",
//                                         @"erreur",
//                                         @"errors",
//                                         @"everfi",
//                                         @"errar",
//                                         @"every",
//                                         @"ever ever",
//                                         @"error error",
//                                         @"Terror",
//                                         @"airer",
//                                         @"ever",
//                                         @"heir",
//                                         @"err",
//                                         @"airer",
//                                         @"airfare",
//                                         @"Ayr",
//                                         @"Ayer",
//
//                                         ],
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"er"
//                                 }];
//
    
        [soundsArray addObject:@{@"id" : @(72),
                                 @"word" : @"ar \n (ex: star)",
                                 @"options": @[
                                         @"Ar",
                                         @"Are",
                                         @"our",
                                         ],
                                 @"sound" : @"Ar",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"ar_ex_star"
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(73),
//                                 @"word" : @"ar \n (ex : dollar)",
//                                 @"sound" : @"Er and Ur and Ir and Or and Ar",
//                                 @"options": @[
//                                         @"Er",
//                                         @"ir",
//                                         @"air",
//                                         @"her",
//                                         @"eal",
//                                         @"erreur",
//                                         @"errors",
//                                         @"everfi",
//                                         @"errar",
//                                         @"every",
//                                         @"ever ever",
//                                         @"error error",
//                                         @"Terror",
//                                         @"airer",
//                                         @"ever",
//                                         @"heir",
//                                         @"err",
//                                         @"airer",
//                                         @"airfare",
//                                         @"Ayr",
//                                         @"Ayer",
//
//                                         ],
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ar_ex_dollar"
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(74),
//                                 @"word" : @"ar \n (ex : warm)",
//                                 @"sound" : @"Or and Ar",
//                                 @"options": @[
//                                         @"Or",
//                                         @"all",
//                                         ],
//                                 @"type" : @"urok",
//                                 @"databaseValue" : @"ar_ex_warm"
//
//                                 }];
    
        //        ********Diphtongs********
        
        [soundsArray addObject:@{@"id" : @(75),
                                 @"word" : @"oi",
                                 @"sound" : @"Oy and Oi",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"oi",
                                 @"options": @[
                                         @"oy",
                                         @"oye",
                                         @"oyee",
                                         @"or",
                                         @"holy",
                                         @"oie",
                                         @"Ali",
                                         @"o",
                                         @"only",
                                         @"oae",
                                         @"08",
                                         @"oe",
                                         @"ou",
                                         @"Zoey",
                                         @"OA",
                                         @"Bowie",
                                         @"Joey",
                                         @"Zoe",
                                         
                                         ]}];
        
//        [soundsArray addObject:@{@"id" : @(76),
//                                 @"word" : @"oy",
//                                 @"sound" : @"Oy and Oi",
//                                 @"type" : @"urok",
//                                 @"databaseValue" : @"oy",
//                                 @"options": @[
//                                         @"oy",
//                                         @"oye",
//                                         @"oyee",
//                                         @"or",
//                                         @"holy",
//                                         @"oie",
//                                         @"Ali",
//                                         @"o",
//                                         @"only",
//                                         @"oae",
//                                         @"08",
//                                         @"oe",
//                                         @"ou",
//                                         @"Zoey",
//                                         @"OA",
//                                         @"Bowie",
//                                         @"Joey",
//                                         @"Zoe",
//                                         ]}];
    
        [soundsArray addObject:@{@"id" : @(77),
                                 @"word" : @"au",
                                 @"wordMatch" :@[@"a"],
                                 @"sound" : @"Au and Aw",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"au",
                                 @"options": @[@"Are",@"oh"]
                                 
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(78),
//                                 @"word" : @"aw",
//                                 @"wordMatch" :@[@"a"],
//                                 @"sound" : @"Au and Aw",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"aw",
//                                 @"options": @[@"Are",@"oh"]
//
//                                 }];
    
        
        [soundsArray addObject:@{@"id" : @(79),
                                 @"word" : @"ou \n (ex : out)",
                                 @"wordMatch" :@[@"o"],
                                 @"sound" : @"Ou and Ow",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"ou_ex_out"
                                 }];
        
        [soundsArray addObject:@{@"id" : @(80),
                                 @"word" : @"ou \n (ex : soup)",
                                 @"wordMatch" :@[@"o"],
                                 @"sound" : @"Oo and Ow and Ui and U_e and Ew",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"ou_ex_soup",
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(81),
//                                 @"word" : @"ow \n (ex : cow)",
//                                 @"wordMatch" :@[@"o"],
//                                 @"sound" : @"Ou and Ow",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ow_ex_cow"
//
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(82),
//                                 @"word" : @"ow \n (ex : show)",
//                                 @"sound" : @"O hard and Ow",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ow_ex_show"
//                                 }];
    
        [soundsArray addObject:@{@"id" : @(83),
                                 @"word" : @"oo \n (ex : book)",
                                 @"wordMatch" :@[@"o"],
                                 @"sound" : @"Oo",
                                 @"type" : @"shortalgorithm",
                                 @"databaseValue" : @"oo_ex_book",
                                 @"options": @[@"O"]
                                 
                                 }];
        
//        [soundsArray addObject:@{@"id" : @(84),
//                                 @"word" : @"oo \n (ex : moon)",
//                                 @"wordMatch" :@[@"o"],
//                                 @"sound" : @"Oo and Ow and Ui and U_e and Ew",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"oo_ex_moon"
//
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(85),
//                                 @"word" : @"u_e \n (ex : flute)",
//                                 @"wordMatch" :@[@"o"],
//                                 @"sound" : @"Oo and Ow and Ui and U_e and Ew",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"u_e_ex_flute"
//
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(86),
//                                 @"word" : @"u_e \n (ex : cute)",
//                                 @"sound" : @"U",
//                                 @"type" : @"urok",
//                                 @"databaseValue" : @"u_e_ex_cute",
//                                 @"options": @[
//                                         @"u",
//                                         @"you"]
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(87),
//                                 @"word" : @"ui",
//                                 @"wordMatch" :@[@"o"],
//                                 @"sound" : @"Oo and Ow and Ui and U_e and Ew",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ui"
//
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(88),
//                                 @"word" : @"ue \n (ex : blue)",
//                                 @"wordMatch" :@[@"o"],
//                                 @"sound" : @"Oo and Ow and Ui and U_e and Ew",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ue_ex_blue"
//
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(89),
//                                 @"word" : @"ue \n (ex : cue)",
//                                 @"sound" : @"U",
//                                 @"type" : @"urok",
//                                 @"databaseValue" : @"ue_ex_cue",
//                                 @"options": @[
//                                         @"u",
//                                         @"you"]
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(90),
//                                 @"word" : @"ew \n (ex : flew)",
//                                 @"wordMatch" :@[@"o"],
//                                 @"sound" : @"Oo and Ow and Ui and U_e and Ew",
//                                 @"type" : @"shortalgorithm",
//                                 @"databaseValue" : @"ew_ex_flew"
//                                 }];
//
//        [soundsArray addObject:@{@"id" : @(91),
//                                 @"word" : @"ew \n (ex : few)",
//                                 @"sound" : @"U",
//                                 @"type" : @"urok",
//                                 @"databaseValue" : @"ew_ex_few",
//                                 @"options": @[
//                                         @"u",
//                                         @"you"]
//                                 }];
    
        //   ******Word beginings and endings*********
        
        //   ********working********
        
        [soundsArray addObject:@{@"id" : @(92),
                                 @"word" : @"pre",
                                 @"sound" :  @"Pre",
                                 @"databaseValue" : @"pre",
                                 @"options": @[
                                         @"pre",
                                         ],
                                 @"type" : @"urok"
                                 }];
        [soundsArray addObject:@{@"id" : @(93),
                                 @"word" : @"be",
                                 @"sound" :  @"Be",
                                 @"databaseValue" : @"be",
                                 @"options": @[
                                         @"Be",
                                         @"b",
                                         ],
                                 @"type" : @"urok"
                                 }];
        [soundsArray addObject:@{@"id" : @(94),
                                 @"word" : @"un",
                                 @"sound" :  @"Un",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"un",
                                 @"options": @[
                                         @"an",
                                         @"on",
                                         ]}];
        
        [soundsArray addObject:@{@"id" : @(95),
                                 @"word" : @"dis",
                                 @"sound" :  @"Dis",
                                 @"databaseValue" : @"dis",
                                 @"options": @[
                                         @"Dis",
                                         @"Ds"],
                                 @"type" : @"urok"
                                 }];
        
        [soundsArray addObject:@{@"id" : @(96),
                                 @"word" : @"under",
                                 @"sound" :  @"Under",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"under",
                                 @"options": @[
                                         @"under"
                                         ]}];
        
        [soundsArray addObject:@{@"id" : @(97),
                                 @"word" : @"sub",
                                 @"sound" :  @"Sub",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"sub",
                                 @"options": @[
                                         @"sub"
                                         ]}];
        
        [soundsArray addObject:@{@"id" : @(98),
                                 @"word" : @"in",
                                 @"sound" :  @"In",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"in_",
                                 @"options": @[
                                         @"In"
                                         ]}];
        
        [soundsArray addObject:@{@"id" : @(99),
                                 @"word" : @"mis",
                                 @"sound" : @"Mis",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"mis",
                                 @"options": @[
                                         @"Mis",
                                         @"miss"
                                         ]}];
        
        [soundsArray addObject:@{@"id" : @(100),
                                 @"word" : @"out",
                                 @"sound" : @"Out",
                                 @"type" : @"urok",
                                 @"databaseValue" : @"out",
                                 @"options": @[
                                         @"Out"
                                         ]}];
        
        [soundsArray addObject:@{@"id" : @(101),
                                 @"word" : @"fore",
                                 @"sound" : @"Fore",
                                 @"databaseValue" : @"fore",
                                 @"options": @[
                                         @"for",
                                         @"fall",
                                         @"four",
                                         @"fore",
                                         @"foar",
                                         @"ford",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(102),
                                 @"word" : @"de",
                                 @"sound" : @"De",
                                 @"databaseValue" : @"de",
                                 @"options": @[
                                         @"De",
                                         @"d",
                                         ],
                                 @"type" : @"urok"}];
        [soundsArray addObject:@{@"id" : @(103),
                                 @"word" : @"re",
                                 @"sound" : @"Re",
                                 @"databaseValue" : @"re",
                                 @"options": @[
                                         @"re",
                                         @"ree",
                                         @"ri",
                                         ],
                                 @"type" : @"urok"}];
        
        
        [soundsArray addObject:@{@"id" : @(104),
                                 @"word" : @"ly",
                                 @"sound" : @"Ly",
                                 @"databaseValue" : @"ly",
                                 @"options": @[
                                         @"Ly",
                                         @"li",
                                         @"lee",
                                         @"le",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(105),
                                 @"word" : @"less",
                                 @"sound" : @"Less",
                                 @"databaseValue" : @"less",
                                 @"options": @[
                                         @"Less",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(106),
                                 @"word" : @"ment",
                                 @"sound" : @"Ment",
                                 @"databaseValue" : @"ment",
                                 @"options": @[
                                         @"Ment",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(107),
                                 @"word" : @"ful",
                                 @"sound" : @"Ful",
                                 @"databaseValue" : @"ful",
                                 @"options": @[
                                         @"Ful",
                                         @"for",
                                         @"full",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(108),
                                 @"word" : @"ness",
                                 @"sound" : @"Ness",
                                 @"databaseValue" : @"ness",
                                 @"options": @[
                                         @"ness",
                                         @"nes",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(109),
                                 @"word" : @"en",
                                 @"sound" : @"En",
                                 @"databaseValue" : @"en",
                                 @"options": @[
                                         @"En",
                                         @"n",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(110),
                                 @"word" : @"ing",
                                 @"sound" : @"Ing",
                                 @"databaseValue" : @"ing",
                                 @"options": @[
                                         @"Ing",
                                         @"Inge",
                                         @"ng",
                                         @"Ink",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(111),
                                 @"word" : @"est",
                                 @"sound" : @"Est",
                                 @"databaseValue" : @"est",
                                 @"options": @[
                                         @"zest",
                                         @"first",
                                         @"fist",
                                         @"chest",
                                         @"Ist",
                                         @"vest",
                                         @"test",
                                         @"pest",
                                         @"ehst",
                                         @"best",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(112),
                                 @"word" : @"able",
                                 @"sound" : @"Able",
                                 @"databaseValue" : @"able",
                                 @"options": @[
                                         @"hubbell",
                                         @"abl",
                                         @"abal",
                                         @"abul",
                                         @"abel",
                                         @"abil",
                                         @"bubble",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(113),
                                 @"word" : @"age",
                                 @"sound" : @"Age",
                                 @"databaseValue" : @"age",
                                 @"options": @[
                                         @"Age",
                                         @"edge",
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(114),
                                 @"word" : @"ed \n (ex : landed)",
                                 @"sound" : @"Ed",
                                 @"databaseValue" : @"ed_ex_landed",
                                 @"options": @[
                                         @"Ed",
                                         @"add",
                                         ],
                                 @"type" : @"urok"}];
        
//        [soundsArray addObject:@{@"id" : @(115),
//                                 @"word" : @"ed \n (ex : snowed)",
//                                 @"wordMatch" :@[@"o"],
//                                 @"sound" : @"D and Ed",
//                                 @"databaseValue" : @"ed_ex_snowed",
//                                 @"type" : @"shortalgorithm",
//                                 @"options": @[@"Did", @"bad", @"day", @"done",@"do", @"dick", @"day", @"the"]
//
//                                 }];
    
//        [soundsArray addObject:@{@"id" : @(116),
//                                 @"word" : @"ed \n (ex : fixed)",
//                                 @"wordMatch" :@[@"o"],
//                                 @"sound" : @"D and Ed",
//                                 @"databaseValue" : @"ed_ex_fixed",
//                                 @"type" : @"shortalgorithm",
//                                 @"options": @[@"It", @"at", @"eight"]
//                                 }];
    
        [soundsArray addObject:@{@"id" : @(117),
                                 @"word" : @"tion",
                                 @"sound" : @"Tion",
                                 @"databaseValue" : @"tion",
                                 @"options": @[
                                         @"sion",
                                         @"shun",
                                         @"shn",
                                         @"shon"],
                                 @"type" : @"urok"}];
        
//        [soundsArray addObject:@{@"id" : @(118),
//                                 @"word" : @"sion \n (ex : mansion)",
//                                 @"sound" : @"Tion",
//                                 @"databaseValue" : @"sion_ex_mansion",
//                                 @"options": @[
//                                         @"sion",
//                                         @"shun",
//                                         @"shn",
//                                         @"shon"],
//                                 @"type" : @"urok"}];
    
        [soundsArray addObject:@{@"id" : @(119),
                                 @"word" : @"sion \n (ex : division)",
                                 @"sound" : @"Sion",
                                 @"databaseValue" : @"sion_ex_divison",
                                 @"options": @[
                                         @"sheon",
                                         @"shun",
                                         @"shn",
                                         @"shon"],
                                 @"type" : @"urok"}];
        
        
        [soundsArray addObject:@{@"id" : @(120),
                                 @"word" : @"ture",
                                 @"sound" : @"Ture",
                                 @"databaseValue" : @"ture",
                                 @"options": @[
                                         @"tchr",
                                         @"chor",
                                         @"chur",
                                         @"Chester",
                                         @"true",
                                         @"chore",
                                         @"cherry",
                                         @"picture",
                                         @"chowder",
                                         @"cheddar",
                                         @"treasure",
                                         @"Sher",
                                         @"chr"],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(121),
                                 @"word" : @"ous",
                                 @"sound" : @"Ous",
                                 @"databaseValue" : @"ous",
                                 @"options": @[
                                         @"Ous",
                                         @"us"
                                         ],
                                 @"type" : @"urok"}];
        
        [soundsArray addObject:@{@"id" : @(122),
                                 @"word" : @"es",
                                 @"sound" : @"Es",
                                 @"databaseValue" : @"es",
                                 @"options": @[
                                         @"Es",
                                         @"s",
                                         @"is",
                                         @"Ez",
                                         @"Iz",
                                         ],
                                 @"type" : @"urok"}];
        
        
    } else {
        [soundsArray addObject:@{@"id" : @(61),
                                 @"word" : @"Hello World",
                                 @"sound": @"Hello World",
                                 @"options": @[
                                         @"Hello World"
                                         ]}];
    }
    
    return soundsArray;
}
#pragma mark - Player Methods
-(void)playSoundAtIndex:(uint)index{
    if (self.soundsArray.count > 0 && self.soundsArray.count >= index+1){
        NSDictionary *songInfo = self.soundsArray[index];
        self.currentSound = songInfo;
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:songInfo[@"sound"] ofType: @"mp3"]; //
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
        if (fileURL)
            [self playSoundFromURL:fileURL];
    }
}
-(void)playSoundFromURL:(NSURL *)url{
    NSError* error;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:&error];
    if (error) {
        //[self playMessageSound:@"Hello World"];
    }
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    _audioPlayer.numberOfLoops = 0; //infinite loop
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
    
}
-(void)stopPlaying{
    if([_audioPlayer isPlaying]){
        [_audioPlayer stop];
    }
}
#pragma mark -
#pragma mark - Recorder Methods
-(void)initializeRecorderForSoundId:(uint)soundType andOffset:(int)offset{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error;
    NSDictionary *recordSettings = @{AVSampleRateKey:@44100.0,
                                     AVFormatIDKey:@(kAudioFormatLinearPCM),
                                     AVNumberOfChannelsKey:@1,
                                     AVEncoderAudioQualityKey:@(AVAudioQualityMedium)
                                     };
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if (error){
        NSLog(@"Couldn't update audio session category");
    }else{
        // changeMade addProperOffset
        NSString *strPath = [bridgeClass getDetectPath1:@"Home Mode" recordSoundType:soundType andOffset:offset];
        strPath = [strPath stringByReplacingOccurrencesOfString:@".dat" withString:@".wav"];
        NSLog(@"SoundUtil Path : %@",strPath);
        recorder = [[AVAudioRecorder alloc]initWithURL:[NSURL fileURLWithPath:strPath]  settings:recordSettings error:&error];
        if (error){
            NSLog(@"Recorder couldn't initialize");
        }else{
            [recorder prepareToRecord];
            [bridgeClass currentDetectionData];
        }
    }
}

-(void)startRecording:(UIViewController *)VC AndOffset:(int)offsetIndex{
    offset = offsetIndex;
    if (!recorder.isRecording) {
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *error;
        [audioSession setActive:true error:&error];
        if (!error){
            [recorder record];
            NSLog(@"SoundUtil Recording : Done");
            recorder.meteringEnabled = true;
            if([self isRecording]){
                return;
            }
            if([bridgeClass get_g_pDetectMgr_isnill] != false){
                int indexOfCurrentSound = [self indexOfCurrentSound];
                NSLog(@"*****index of sound while Recording %d******",indexOfCurrentSound);
                NSString *recordingPath = [bridgeClass getRecordPath:indexOfCurrentSound andOffset:offsetIndex]; //changeMade
                NSLog(@"Recording path Start: %@",recordingPath);
                [bridgeClass g_pDetectMgr_RecordStart:VC :recordingPath];
                isRecording = true;
            }
            
        }else{
            NSLog(@"Audio Session couldn't be activated!");
        }
    }
}
-(void)stopRecordingWithOffset:(int)offsetIndex{
    [recorder stop];
    offset = offsetIndex;
    int indexOfCurrentSound = (int)[self.soundsArray indexOfObject:self.currentSound];
    NSString *recordingPath = [bridgeClass getRecordPath:indexOfCurrentSound andOffset:offsetIndex]; //changeMade
    BOOL isProcessingDataAddedSuccessfully = [bridgeClass m_curDetectingData_AddProcessData:recordingPath];
    isRecording = false;
    if (isProcessingDataAddedSuccessfully){
        if([bridgeClass m_curDetecting_Data_GetRecordedCnt] == true){
            if([bridgeClass m_curDetecting_ExtractDetectingData] == true)
            {
                NSLog(@"*****index of sound while saving %d******",indexOfCurrentSound);
                
                [bridgeClass saveRecordedData:indexOfCurrentSound AndOffset:offsetIndex];
            }
        }
    }
    //self.gotoNextScreen()
}

-(void)switchRecordingStatus:(UIViewController *)VC AndOffset:(int)offsetIndex{
    offset = offsetIndex;
    if (isRecording){
        [self stopRecordingWithOffset : offsetIndex];
    }else{
        [self startRecording:VC AndOffset : offsetIndex];
    }
    
}
#pragma mark - Helper Methods
-(BOOL)isRecording{
    return isRecording;
}
-(int)indexOfCurrentSound{
    return (int)[self.soundsArray indexOfObject:self.currentSound];
}
-(int)getOffset{
    return offset;
}
-(void)stopRecorderAndOffset:(int)offsetIndex{
    NSLog(@"stopRecorderAndOffset");
    
    isRecording = false;
    if([bridgeClass get_g_pDetectMgr_isnill] != false){
        int indexOfCurrentSound = [self indexOfCurrentSound];
        NSLog(@"*****index of sound while Recording %d******",indexOfCurrentSound);
        NSString *recordingPath = [bridgeClass getRecordPath:indexOfCurrentSound andOffset:offsetIndex]; //changeMade
        NSLog(@"Recording path Stop: %@",recordingPath);
        //   isRecording = true;
        [bridgeClass g_pDetectMgr_RecordStopWithoutSavingAnyFile:recordingPath];
    }
    [recorder stop];
}
-(void)startRecordingForEqualizer{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error;
    [audioSession setActive:true error:&error];
    if (!error){
        [recorder record];
        NSLog(@"SoundUtil Recording : Done1");
        recorder.meteringEnabled = true;
    }
}
-(void)stoprRecordingForEqualizer{
    NSLog(@"stoprRecordingForEqualizer");
    [recorder stop];
}
-(void)onlyStopRecordingAndSavedUnProcessedFile:(int)offsetIndex{
    [recorder stop];
    offset = offsetIndex;
    int indexOfCurrentSound = (int)[self.soundsArray indexOfObject:self.currentSound];
    isRecording = false;
    NSLog(@"*****Recorder Stopped %d******",indexOfCurrentSound);
    // [bridgeClass saveRecordedData:indexOfCurrentSound AndOffset:offsetIndex];
}
@end

