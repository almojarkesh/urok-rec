//
//  IndexCell.h
//  urok
//
//  Created by Pavan Jangid on 06/05/18.
//  Copyright © 2018 Accuretech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndexCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
